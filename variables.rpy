init python:
    numOfUsers = 0
    userSat = 100
    dataLeak = 0
    revenue = 10000

    tables = [
    {'name': 'Payment',
    'popularity': '3',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Customer Name', 'count': '15'},{'name': 'Credit Card No.', 'count': '15'},{'name': 'Driver ID', 'count': '10'}]},
    {'name': 'Trip',
    'popularity': '3',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Rider ID', 'count': '10'},{'name': 'Driver ID', 'count': '10'},{'name': 'Cab ID', 'count': '10'},{'name': 'Start location', 'count': '10'},{'name': 'End location', 'count': '10'},{'name': 'Trip start time', 'count': '10'},{'name': 'Trip end time', 'count': '10'}]},
    {'name': 'Rider',
    'popularity': '2',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Rider ID', 'count': '10'},{'name': 'Name', 'count': '15'},{'name': 'Joined Date', 'count': '5'},{'name': 'Gender', 'count': '5'},{'name': 'Phone number', 'count': '15'},{'name': 'Payment account', 'count': '15'}]},
    {'name': 'Driver',
    'popularity': '2',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Driver ID', 'count': '10'},{'name': 'Name', 'count': '15'},{'name': 'Joined Date', 'count': '5'},{'name': 'Cab ID', 'count': '10'}]},
    {'name': 'Cab',
    'popularity': '2',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Cab ID', 'count': '10'},{'name': 'Brand', 'count': '5'},{'name': 'Model', 'count': '5'},{'name': 'Type', 'count': '5'},{'name': 'Reg No', 'count': '10'}]},
    {'name': 'Rider Bookmarks',
    'popularity': '1',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Bookmark ID', 'count': '10'},{'name': 'Rider ID', 'count': '10'},{'name': 'Bookmark Image', 'count': '10'},{'name': 'Bookmark Details', 'count': '10'}]},
    {'name': 'Location',
    'popularity': '2',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Location ID', 'count': '10'},{'name': 'Map Grid ID', 'count': '10'},{'name': 'Lankmark', 'count': '5'},{'name': 'Landmark Name', 'count': '5'},{'name': 'Related Location', 'count': '5'}]},
    {'name': 'Map Grid',
    'popularity': '1',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Map Grid ID', 'count': '5'},{'name': 'Latitude', 'count': '5'},{'name': 'Longtitude', 'count': '5'}]},
    {'name': 'Assurance',
    'popularity': '1',
    'malware': 'clean',
    'sensitivity': '50',
    'items':[{'name': 'Assurance ID', 'count': '10'},{'name': 'Rider ID', 'count': '10'},{'name': 'Driver ID', 'count': '10'},{'name': 'Start location', 'count': '10'},{'name': 'End location', 'count': '10'}]},
    ]

    members = [
    {'name': 'Tommy',
    'image': 'staff_tommy.png',
    'red_dot': 'staff_ben_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '4'},{'index': 'DB & infra', 'score': '5'},{'index': 'Security & Compliance', 'score': '4'},{'index': 'Testing & Quality Assurance', 'score': '5'},{'index': 'Customer comm', 'score': '3'}]},
    {'name': 'Clara',
    'image': 'staff_clara.png',
    'red_dot': 'staff_clara_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '5'},{'index': 'DB & infra', 'score': '4'},{'index': 'Security & Compliance', 'score': '4'},{'index': 'Testing & Quality Assurance', 'score': '4'},{'index': 'Customer comm', 'score': '4'}]},
    {'name': 'Ben',
    'image': 'staff_ben.png',
    'red_dot': 'staff_ben_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '4'},{'index': 'DB & infra', 'score': '3'},{'index': 'Security & Compliance', 'score': '5'},{'index': 'Testing & Quality Assurance', 'score': '3'},{'index': 'Customer comm', 'score': '3'}]},
    {'name': 'Rebecca',
    'image': 'staff_rebecca.png',
    'red_dot': 'staff_rebecca_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '3'},{'index': 'DB & infra', 'score': '4'},{'index': 'Security & Compliance', 'score': '4'},{'index': 'Testing & Quality Assurance', 'score': '3'},{'index': 'Customer comm', 'score': '5'}]},
    {'name': 'Joseph',
    'image': 'staff_joseph.png',
    'red_dot': 'staff_joseph_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '3'},{'index': 'DB & infra', 'score': '3'},{'index': 'Security & Compliance', 'score': '4'},{'index': 'Testing & Quality Assurance', 'score': '3'},{'index': 'Customer comm', 'score': '3'}]},
    {'name': 'Peter',
    'image': 'staff_peter.png',
    'red_dot': 'staff_peter_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '5'},{'index': 'DB & infra', 'score': '5'},{'index': 'Security & Compliance', 'score': '4'},{'index': 'Testing & Quality Assurance', 'score': '3'},{'index': 'Customer comm', 'score': '3'}]},
    {'name': 'Mandy',
    'image': 'staff_mandy.png',
    'red_dot': 'staff_mandy_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '5'},{'index': 'DB & infra', 'score': '4'},{'index': 'Security & Compliance', 'score': '5'},{'index': 'Testing & Quality Assurance', 'score': '5'},{'index': 'Customer comm', 'score': '2'}]},
    {'name': 'Lex',
    'image': 'staff_lex.png',
    'red_dot': 'staff_lex_red.png',
    'pwStrength': 0,
    'capability': [{'index': 'Programming', 'score': '4'},{'index': 'DB & infra', 'score': '5'},{'index': 'Security & Compliance', 'score': '3'},{'index': 'Testing & Quality Assurance', 'score': '3'},{'index': 'Customer comm', 'score': '4'}]}]


    def checkIndicators():
        globals()['numOfUsers'] = max(0, numOfUsers)
        globals()['userSat'] = min(max(0, userSat), 100)
        globals()['dataLeak'] = max(0, dataLeak)
        globals()['revenue'] = max(0, revenue)

    playerSen = 0
    cal_sensitive = []

    feature_popularity = 0

    def ISD(featureName):
        featureIndex = int(findIndex(startProgress_str, tables, "name")[0])
        for attribute in sensitive[featureIndex]:
            if attribute != "sensitive_grey.png":
                return True


    attributeSen = 0
    def dataLeakCal():
        cal_sensitive.clear()
        globals()['attributeSen'] = 0
        for a in range(len(tables)):
            if tables[a]['name']==DBpopUp_str:
                for b in range(len(tables[a]['items'])):
                    cal_sensitive.append(sensitive[a][b])
                
                for j in range(len(cal_sensitive)):
                    if cal_sensitive[j]== "sensitive_red.png":
                        globals()['playerSen'] = 15
                    elif cal_sensitive[j]== "sensitive_yellow.png":
                        globals()['playerSen'] = 10
                    elif cal_sensitive[j]== "sensitive_green.png":
                        globals()['playerSen'] = 5
                    globals()['attributeSen'] += abs(playerSen - int(tables[a]['items'][j]['count']))
                tables[a]['sensitivity'] = str(attributeSen)
        addRounds()


    def numOfUsersCal():
        if len(publishedFeatures) > 0:
            if pwFlag == 1 or malwareFlag == 1 or patchFlag == 1:
                globals()['numOfUsers'] -= 20 * len(publishedFeatures)
            else:
                globals()['numOfUsers'] += 20 * len(publishedFeatures)
            revenueCal()
        checkIndicators()

    def revenueCal():
        globals()['feature_popularity'] = 0
        for i in range(len(publishedFeatures)):
            for j in range(len(tables)):
                if publishedFeatures[i]==tables[j]['name']:
                    globals()['feature_popularity'] += int(tables[j]['popularity'])
        if pwFlag == 1 or malwareFlag == 1 or patchFlag == 1:
            globals()['revenue'] -= (numOfUsers + feature_popularity) * 25
        else:
            globals()['revenue'] += (numOfUsers + feature_popularity) * 10
        checkIndicators()

    def revokeCal(actionRound):
        if not checkMinRoles(): return
        if not checkMemberRatio():
            renpy.show_screen('RatioRecommend')


        if (actionRound - devEventStartRound) <= devEventRunTime and len(deployedEssentials) == len(EssentialFeatures) and len(publishedFeatures) != 0:
            globals()['numOfUsers'] += 30
            globals()['userSat'] += 10
            globals()['revenue'] += (30*10)
            manpowerCal()
        checkIndicators()

    def pwCal(strength): # members' pw stronger, less p to be hacked?
        addRounds()
        if strength<3:
            globals()['numOfUsers'] += 10
            globals()['userSat'] += 10
            # globals()['dataLeak'] += 10
            globals()['revenue'] += 10*5
        else:
            globals()['numOfUsers'] += 20
            globals()['userSat'] += 20
            # globals()['dataLeak'] += 5
            globals()['revenue'] += 10*10
        checkIndicators()

    def malwareCal():
        globals()['numOfUsers'] += 20
        globals()['userSat'] += 20
        # globals()['dataLeak'] += 20
        globals()['revenue'] += 10*10
        addRounds()
        globals()['malwareFlag'] = 0
        for i in range(len(publishedDbs)):
            if publishedDbs[i][0]['name'] == installedDB:
                publishedDbs[i][0]['malware'] == 'treated'
        checkIndicators()

    def rewardCal():
        globals()['numOfUsers'] += 10 * len(publishedFeatures)
        globals()['userSat'] += 15
        globals()['revenue'] += 10 * len(publishedFeatures)
        checkIndicators()

    def dataBreachCal(users,popularity,sensitivity):
        globals()['numOfUsers'] -= users
        globals()['userSat'] -= popularity * 10
        globals()['dataLeak'] += sensitivity
        globals()['revenue'] -= users * 20
        checkIndicators()



define displayScreens = [
    {'name': 'Development', 'display': False},
    {'name': 'Production', 'display': False},
    {'name': 'Team Mgnt', 'display': False}
]


define EssentialFeatures = ['Payment', 'Trip']

screen indicators():
    add im.Scale("indicator_user.png", 25, 25) xalign 0.3 yalign 0.01
    add im.Scale("panel_white.png", 120, 1.5) xalign 0.32 yalign 0.045
    text "{color=#ffffff} [numOfUsers] {/color}" xalign 0.34 yalign 0.016 size 12
    text "{color=#ffffff} No. of users {/color}" xalign 0.33 yalign 0.05 size 12

    add im.Scale("indicator_satisfaction.png", 25, 25) xalign 0.42 yalign 0.01
    add im.Scale("panel_white.png", 120, 1.5) xalign 0.45 yalign 0.045
    text "{color=#ffffff} [userSat] {/color}" xalign 0.46 yalign 0.016 size 12
    text "{color=#ffffff} Users satisfaction {/color}" xalign 0.45 yalign 0.05 size 12

    add im.Scale("indicator_leakage.png", 25, 25) xalign 0.54 yalign 0.01
    add im.Scale("panel_white.png", 120, 1.5) xalign 0.58 yalign 0.045
    text "{color=#ffffff} [dataLeak] {/color}" xalign 0.58 yalign 0.016 size 12
    text "{color=#ffffff} No. of records exposed {/color}" xalign 0.58 yalign 0.05 size 12

    add im.Scale("indicator_revenue.png", 25, 25) xalign 0.66 yalign 0.01
    add im.Scale("panel_white.png", 120, 1.5) xalign 0.71 yalign 0.045
    text "{color=#ffffff} [revenue] {/color}" xalign 0.71 yalign 0.016 size 12
    text "{color=#ffffff} Budget {/color}" xalign 0.70 yalign 0.05 size 12
