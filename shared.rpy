init python:
    DBpopUp_str = ''
    DBpopUp_int = 0

    sensitive = [["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"],
                ["sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png","sensitive_grey.png"]]
    
    tempDB = []
    developingDbs = []

    temp_count = 0

    permission = [
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"],
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"],
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"],
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"],
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"],
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"],
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"],
                ["unchecked.png","unchecked.png","unchecked.png","unchecked.png"]]


    def DBpopUp(i, name):
        globals()['DBpopUp_int'] = i
        globals()['DBpopUp_str'] = name
        renpy.show_screen('DBDoPopWindow')


    def sensitiveDropDown(x):
        renpy.show_screen('sen_dropdown'+str(x))

    def closeAllSenDropdown():
        for i in range(7):
            renpy.hide_screen('sen_dropdown'+str(i))

    def changeSensitive(item,color):
        for z in range(len(tables)):
            if tables[z]['name']==DBpopUp_str:
                dbIndex = z
        if color==0:
            sensitive[dbIndex][item]="sensitive_red.png"
        if color==1:
            sensitive[dbIndex][item]="sensitive_yellow.png"
        if color==2:
            sensitive[dbIndex][item]="sensitive_green.png"

    role_show = 0
    def role_trigger_popUp(member_index):
        globals()['role_show'] = member_index
        renpy.show_screen('role_popUp')
    
    #role_pw = ""
    role_pw0 = ""
    role_pw1 = ""
    role_pw2 = ""
    role_pw3 = ""
    role_pw4 = ""
    role_pw5 = ""
    role_pw6 = ""
    role_pw7 = ""

    role_strength = 0
    def role_checkPW():
        lower = 0
        upper = 0
        number = 0
        char = 0
        length = 0

        lower = any(not a.islower() for a in globals()['role_pw'+str(role_show)])
        upper = any(not b.isupper() for b in globals()['role_pw'+str(role_show)])
        number = any(not c.isnumeric() for c in globals()['role_pw'+str(role_show)])
        char = any(not d.isalnum() for d in globals()['role_pw'+str(role_show)])
        leng = len(globals()['role_pw'+str(role_show)])>8

        globals()['role_strength'] = lower+upper+number+char+leng
        renpy.show_screen('role_pwStrength')
        globals()['changePWCount'] += 1
        members[role_show]['pwStrength'] = role_strength

        if members[role_show]['name'] == hackedMember and pwFlag == 1:
            globals()['pwFlag'] = 0
            pwCal(members[role_show]['pwStrength'])

    def checkbox(member_index, permission_index):
        if permission[member_index][permission_index] == "unchecked.png":
            permission[member_index][permission_index] = "checked.png"
        else:
            permission[member_index][permission_index] = "unchecked.png"

screen shared():
    # Task Progress
    text "{color=#000000} Task Progress {/color}" xalign 0.02 yalign 0.16 size 20

    # Roles
    text "{color=#000000} Roles {/color}" xalign 0.545 yalign 0.16 size 20

    # member icons
    hbox:
        xalign 0.52 yalign 0.23
        xanchor 0 yanchor 0
        spacing 10
        for i in range(len(teamList)):
            if displayScreens[0]['display'] and teamList[i] == 'Development':
                vbox:
                    xminimum 80
                    if hackedMember == members[i]['name'] and pwFlag == 1:
                        imagebutton idle im.Scale(members[i]['red_dot'], 35, 35) xpos 20 action Function(role_trigger_popUp,i)
                    else:
                        imagebutton idle im.Scale(members[i]['image'], 35, 35) xpos 20 action Function(role_trigger_popUp,i)
                    text "{color=#2B547E} %s {/color}"%members[i]['name'] size 14 xpos 14 ypos 5
                    if roleList[i] == 'DBA':
                        text "{color=#E77471}{b} DBA {/b}{/color}" size 14 xpos 20 ypos 10
                    else:
                        text "{color=#566D7E}{b} Developer {/b}{/color}" size 14 ypos 10
            if displayScreens[1]['display'] and teamList[i] == 'Production':
                vbox:
                    xminimum 80
                    if hackedMember == members[i]['name'] and pwFlag == 1:
                        imagebutton idle im.Scale(members[i]['red_dot'], 35, 35) xpos 20 action Function(role_trigger_popUp,i)
                    else:
                        imagebutton idle im.Scale(members[i]['image'], 35, 35) xpos 20 action Function(role_trigger_popUp,i)
                    text "{color=#2B547E} %s {/color}"%members[i]['name'] size 14 xpos 14 ypos 5
                    if roleList[i] == 'DBA':
                        text "{color=#E77471}{b} DBA {/b}{/color}" size 14 xpos 20 ypos 10
                    else:
                        text "{color=#566D7E}{b} Developer {/b}{/color}" size 14 ypos 10





    # Databases
    text "{color=#000000} Databases {/color}" xalign 0.57 yalign 0.39 size 20

    if displayDBs:

        $ tempDB = []
        if displayScreens[0]['display']:
            for developing in developingDbs:
                for feature in tables:
                    if developing == feature['name']:
                        $ tempDB.append(feature)
        if displayScreens[1]['display']:
            for published in publishedDbs:
                for feature in tables:
                    if published[0]['name'] == feature['name']:
                        $ tempDB.append(feature)



        hbox:
            xalign 0.51
            yalign 0.55
            xanchor 0
            yanchor 0
            xminimum 100
            spacing 30
            
            $ temp_count = 0
            for i in tempDB[:4]:
                vbox:
                    spacing 3
                    if disableRounds:
                        imagebutton idle im.Scale("database_disabled.png", 130, 120) ypos -60 action NullAction()
                    elif patchFlag == 1 and i['name'] == installedDB:
                        imagebutton idle im.Scale("database_alert.png", 130, 120) ypos -60 action Function(DBpopUp, temp_count, i['name'])
                    elif malwareFlag == 1 and i['name'] == installedDB:
                        imagebutton idle im.Scale("database_alert.png", 130, 120) ypos -60 action Function(DBpopUp, temp_count, i['name'])
                    else:
                        imagebutton idle im.Scale("database.png", 130, 120) ypos -60 action Function(DBpopUp, temp_count, i['name'])
                    text "{color=#000000} [i[name]] {/color}" size 15 xpos 12 ypos -200
                    add im.Scale("panel_black.png", 130, 2) xpos 12 ypos -195
                    for j in i['items']:
                        text "{color=#000000}  -  [j[name]] {/color}" size 10 xpos 12 ypos -190
                    $ temp_count = temp_count + 1

        hbox:
            xalign 0.51
            yalign 0.75
            xanchor 0
            yanchor 0
            xminimum 100
            spacing 30

            $ temp_count = 0
            for i in tempDB[4:8]:
                vbox:
                    spacing 3
                    if disableRounds:
                        imagebutton idle im.Scale("database_disabled.png", 130, 120) ypos -60 action NullAction()
                    elif patchFlag == 1 and i['name'] == installedDB:
                        imagebutton idle im.Scale("database_alert.png", 130, 120) ypos -60 action Function(DBpopUp, temp_count, i['name'])
                    else:
                        imagebutton idle im.Scale("database.png", 130, 120) ypos -60 action Function(DBpopUp, temp_count, i['name'])
                    text "{color=#000000} [i[name]] {/color}" size 15 xpos 12 ypos -200
                    add im.Scale("panel_black.png", 130, 2) xpos 12 ypos -195
                    for j in i['items']:
                        text "{color=#000000}  -  [j[name]] {/color}" size 10 xpos 12 ypos -190
                    $ temp_count = temp_count + 1

        hbox:
            xalign 0.51
            yalign 0.95
            xanchor 0
            yanchor 0
            xminimum 100
            spacing 30

            $ temp_count = 0
            for i in tempDB[8:9]:
                vbox:
                    spacing 3
                    if disableRounds:
                        imagebutton idle im.Scale("database_disabled.png", 130, 120) ypos -60 action NullAction()
                    elif patchFlag == 1 and i['name'] == installedDB:
                        imagebutton idle im.Scale("database_alert.png", 130, 120) ypos -60 action Function(DBpopUp, temp_count, i['name'])
                    else:
                        imagebutton idle im.Scale("database.png", 130, 120) ypos -60 action Function(DBpopUp, temp_count, i['name'])
                    text "{color=#000000} [i[name]] {/color}" size 15 xpos 12 ypos -200
                    add im.Scale("panel_black.png", 130, 2) xpos 12 ypos -195
                    for j in i['items']:
                        text "{color=#000000}  -  [j[name]] {/color}" size 10 xpos 12 ypos -190
                    $ temp_count = temp_count + 1

screen DBDoPopWindow():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()

    if (displayScreens[0]['display']):
        add im.Scale("UI_popup_bkg.png", 500, 530) xalign 0.5 yalign 0.55
        text "{color=#000000} {b} DB - [DBpopUp_str] {/b} {/color}" xalign 0.5 yalign 0.175 size 20
        imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.17 action [Hide('DBDoPopWindow'), Hide('sensitiveDropDown'), Function(closeAllSenDropdown)]
        text "{color=#000000} Please select the encrypted key strength for each attribute {/color}" xalign 0.5 yalign 0.205 size 14
        text "{color=#000000} {color=#FF0000}Red{/color}: 256-bit AES      {color=#E6C000}Yellow{/color}: 128-bit AES     {color=#008000}Green{/color}: Unencrypted {/color}" xalign 0.5 yalign 0.23 size 12
        imagebutton idle im.Scale("UI_popup_confirm.png", 100, 35) xalign 0.5 yalign 0.85 action [Function(dataLeakCal),Hide('DBDoPopWindow'), Hide('sensitiveDropDown'), Function(closeAllSenDropdown)]
        text "{color=#000000} Confirm ✔️ {/color}" xalign 0.5 yalign 0.84 size 14

        vbox:
            xalign 0.001
            yalign 0.220
            xanchor 0
            yanchor 0
            spacing 8

            $ tempIndex = 0
            for m in range(len(tables)):
                if tables[m]['name']==DBpopUp_str:
                    $ tempIndex = m

            for n in range (len(tables[tempIndex]['items'])):
                hbox:
                    add im.Scale("panel_lightgrey.png", 445, 50) xpos 420 ypos 20
                    imagebutton idle im.Scale(sensitive[tempIndex][n], 30, 30) xpos -10 ypos 28 action Function(sensitiveDropDown,n)
                    text "{color=#000000} %s {/color}"%tables[tempIndex]['items'][n]['name'] size 14 xpos 30 ypos 36
    
    if (displayScreens[1]['display']):
        add im.Scale("UI_popup_bkg.png", 500, 530) xalign 0.5 yalign 0.55
        text "{color=#000000} {b} DB - [DBpopUp_str] {/b} {/color}" xalign 0.5 yalign 0.175 size 20
        imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.17 action [Hide('DBDoPopWindow'), Hide('ConfirmPatchTesting'), Hide('PatchDetails')]
        
        text "{color=#000000} Released Patch Versions {/color}" xalign 0.38 yalign 0.28 size 14
        add im.Scale("panel_black.png", 430, 2) xalign 0.49 yalign 0.3

        vbox:
            xanchor 0
            yanchor 0
            xalign 0.33
            yalign 0.32
            spacing 5
            for i in range(len(patches)):
                if deliverPatch[i]:
                    if round >= deliverPatch[i]:
                        hbox:
                            imagebutton idle im.Scale("panel_grey.png", 430, 40) xalign 0 yalign 0 action Function(showPatchDetails, i)
                            text "{color=#000000} • Patch-%s {/color}"%patches[i]['id'] xpos -426 ypos 10 size 14
                            text "{color=#000000} {u} more details {/u} {/color}" xpos -426 ypos 15 size 10 
            
        vbox:
            xanchor 0
            yanchor 0
            xalign 0.58
            yalign 0.379
            spacing 5
            for i in range(len(publishedDbs[DBpopUp_int][0]['patch'])):
                if i < latestPatch:
                    vbox:
                        xanchor 0
                        yanchor 0
                        ypos -47
                        imagebutton idle im.Scale("panel_white.png", 100, 30) xpos 0 ypos 7 action Function (patchStatus, i)
                        if patches[i]['color'] == 'critical':
                            text "{color=[color_critical]} %s {/color}" %publishedDbs[DBpopUp_int][0]['patch'][i][1]['patch-status'] xalign 0.5 ypos -15 size 10
                        elif patches[i]['color'] == 'medium':
                            text "{color=[color_medium]} %s {/color}" %publishedDbs[DBpopUp_int][0]['patch'][i][1]['patch-status'] xalign 0.5 ypos -15 size 10
                        else:
                            text "{color=[color_least]} %s {/color}" %publishedDbs[DBpopUp_int][0]['patch'][i][1]['patch-status'] xalign 0.5 ypos -15 size 10

        if malwareFlag == 1 and DBpopUp_str == installedDB:
            text "{color=#000000} Detected Malware {/color}" xalign 0.38 yalign 0.49 size 14
            add im.Scale("panel_black.png", 430, 2) xalign 0.49 yalign 0.52

            vbox:
                xanchor 0
                yanchor 0
                xalign 0.33
                yalign 0.54
                spacing 5

                hbox:
                    imagebutton idle im.Scale("panel_grey.png", 430, 40) xalign 0 yalign 0 action NullAction()
                    text "{color=#000000} Malware 1 {/color}" xpos -426 ypos 10 size 14
                    imagebutton idle im.Scale("panel_white.png", 100, 30) xpos -190 ypos 6 action [Function(malwareCal),Hide('DBDoPopWindow')]
                    text "{color=#000000} Uninstall {/color}" xpos -270 ypos 10 size 10




screen sen_dropdown0():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('sen_dropdown0')]
    add im.Scale("panel_lightblue.png", 123, 42) xpos 480 ypos 181
    imagebutton idle im.Scale("sensitive_red.png", 30, 30) xpos 488 ypos 186 action [Hide('sen_dropdown0'),Function(changeSensitive,0,0)]
    imagebutton idle im.Scale("sensitive_yellow.png", 30, 30) xpos 523 ypos 186 action [Hide('sen_dropdown0'),Function(changeSensitive,0,1)]
    imagebutton idle im.Scale("sensitive_green.png", 30, 30) xpos 560 ypos 186 action [Hide('sen_dropdown0'),Function(changeSensitive,0,2)]

screen sen_dropdown1():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('sen_dropdown1')]
    add im.Scale("panel_lightblue.png", 123, 42) xpos 480 ypos 239
    imagebutton idle im.Scale("sensitive_red.png", 30, 30) xpos 488 ypos 244 action [Hide('sen_dropdown1'),Function(changeSensitive,1,0)]
    imagebutton idle im.Scale("sensitive_yellow.png", 30, 30) xpos 523 ypos 244 action [Hide('sen_dropdown1'),Function(changeSensitive,1,1)]
    imagebutton idle im.Scale("sensitive_green.png", 30, 30) xpos 560 ypos 244 action [Hide('sen_dropdown1'),Function(changeSensitive,1,2)]

screen sen_dropdown2():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('sen_dropdown2')]
    add im.Scale("panel_lightblue.png", 123, 42) xpos 480 ypos 297
    imagebutton idle im.Scale("sensitive_red.png", 30, 30) xpos 488 ypos 302 action [Hide('sen_dropdown2'),Function(changeSensitive,2,0)]
    imagebutton idle im.Scale("sensitive_yellow.png", 30, 30) xpos 523 ypos 302 action [Hide('sen_dropdown2'),Function(changeSensitive,2,1)]
    imagebutton idle im.Scale("sensitive_green.png", 30, 30) xpos 560 ypos 302 action [Hide('sen_dropdown2'),Function(changeSensitive,2,2)]

screen sen_dropdown3():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('sen_dropdown3')]
    add im.Scale("panel_lightblue.png", 123, 42) xpos 480 ypos 355
    imagebutton idle im.Scale("sensitive_red.png", 30, 30) xpos 488 ypos 360 action [Hide('sen_dropdown3'),Function(changeSensitive,3,0)]
    imagebutton idle im.Scale("sensitive_yellow.png", 30, 30) xpos 523 ypos 360 action [Hide('sen_dropdown3'),Function(changeSensitive,3,1)]
    imagebutton idle im.Scale("sensitive_green.png", 30, 30) xpos 560 ypos 360 action [Hide('sen_dropdown3'),Function(changeSensitive,3,2)]

screen sen_dropdown4():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('sen_dropdown4')]
    add im.Scale("panel_lightblue.png", 123, 42) xpos 480 ypos 413
    imagebutton idle im.Scale("sensitive_red.png", 30, 30) xpos 488 ypos 418 action [Hide('sen_dropdown4'),Function(changeSensitive,4,0)]
    imagebutton idle im.Scale("sensitive_yellow.png", 30, 30) xpos 523 ypos 418 action [Hide('sen_dropdown4'),Function(changeSensitive,4,1)]
    imagebutton idle im.Scale("sensitive_green.png", 30, 30) xpos 560 ypos 418 action [Hide('sen_dropdown4'),Function(changeSensitive,4,2)]

screen sen_dropdown5():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('sen_dropdown5')]
    add im.Scale("panel_lightblue.png", 123, 42) xpos 480 ypos 471
    imagebutton idle im.Scale("sensitive_red.png", 30, 30) xpos 488 ypos 476 action [Hide('sen_dropdown5'),Function(changeSensitive,5,0)]
    imagebutton idle im.Scale("sensitive_yellow.png", 30, 30) xpos 523 ypos 476 action [Hide('sen_dropdown5'),Function(changeSensitive,5,1)]
    imagebutton idle im.Scale("sensitive_green.png", 30, 30) xpos 560 ypos 476 action [Hide('sen_dropdown5'),Function(changeSensitive,5,2)]

screen sen_dropdown6():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('sen_dropdown6')]
    add im.Scale("panel_lightblue.png", 123, 42) xpos 480 ypos 529
    imagebutton idle im.Scale("sensitive_red.png", 30, 30) xpos 488 ypos 534 action [Hide('sen_dropdown6'),Function(changeSensitive,6,0)]
    imagebutton idle im.Scale("sensitive_yellow.png", 30, 30) xpos 523 ypos 534 action [Hide('sen_dropdown6'),Function(changeSensitive,6,1)]
    imagebutton idle im.Scale("sensitive_green.png", 30, 30) xpos 560 ypos 534 action [Hide('sen_dropdown6'),Function(changeSensitive,6,2)]

screen role_popUp():
    add im.Scale("UI_popup_bkg.png", 500, 350) xalign 0.5 yalign 0.2
    text "{color=#000000} Set Password - %s {/color}"%members[role_show]['name'] xalign 0.5 yalign 0.17 size 16
    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.17 action [Hide('role_popUp'),Hide('role_pwStrength')]
    add im.Scale("panel_lightgrey.png", 450, 50) xalign 0.5 yalign 0.25

    input value VariableInputValue("role_pw"+str(role_show)) xalign 0.5 yalign 0.26 size 16
    imagebutton idle im.Scale("panel_lightblue.png", 80, 50) xalign 0.5 yalign 0.35 action Function(role_checkPW)
    text "{color=#000000} Confirm {/color}" xalign 0.5 yalign 0.36 size 16

    vbox:
        xalign 0.35 yalign 0.41
        xanchor 0 yanchor 0
        spacing 5
        text "{color=#000000} • Password combination with {/color}{color=#3366FF}number, upper {/color}" size 16
        text "{color=#3366FF} and lower case letters, length longer than {/color}" size 16
        text "{color=#3366FF} 8 characters{/color}{color=#000000} is recommended {/color}" size 16


    # add im.Scale("panel_black.png", 300, 2) xalign 0.5 yalign 0.55
    # text "{color=#000000} Set Permission {/color}" xalign 0.5 yalign 0.52 size 16

    # imagebutton idle im.Scale(permission[role_show][0], 30, 30) xalign 0.4 yalign 0.60 action Function(checkbox,role_show,0)
    # text "{color=#000000} Select {/color}" xalign 0.5 yalign 0.60 size 16
    # imagebutton idle im.Scale(permission[role_show][1], 30, 30) xalign 0.4 yalign 0.65 action Function(checkbox,role_show,1)
    # text "{color=#000000} Update {/color}" xalign 0.5 yalign 0.65 size 16
    # imagebutton idle im.Scale(permission[role_show][2], 30, 30) xalign 0.4 yalign 0.70 action Function(checkbox,role_show,2)
    # text "{color=#000000} Create {/color}" xalign 0.5 yalign 0.70 size 16
    # imagebutton idle im.Scale(permission[role_show][3], 30, 30) xalign 0.4 yalign 0.75 action Function(checkbox,role_show,3)
    # text "{color=#000000} Delete {/color}" xalign 0.5 yalign 0.75 size 16

    # imagebutton idle im.Scale("panel_lightgreen.png", 80, 50) xalign 0.5 yalign 0.84 action NullAction()
    # text "{color=#000000} Confirm {/color}" xalign 0.5 yalign 0.83 size 16

screen role_pwStrength():
    add im.Scale("panel_white.png", 480, 150) xalign 0.5 yalign 0.4
    text "{color=#000000} Password Strength {/color}" xalign 0.4 yalign 0.32 size 16
    add im.Scale("panel_darkgrey.png", 450, 50) xalign 0.5 yalign 0.37
    if role_strength<3:
        add im.Scale("color_tommy.png", 150, 50) xpos 410 yalign 0.37
    elif role_strength<5:
        add im.Scale("color_peter.png", 300, 50) xpos 410 yalign 0.37
    else:
        add im.Scale("color_lex.png", 450, 50) xpos 410 yalign 0.37
        
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.5 action [Hide("role_pwStrength"), Hide("role_popUp")]
    text "{color=#ffffff} {b}Close{/b} {/color}" xalign 0.5 yalign 0.5 size 16
    