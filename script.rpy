﻿init python:
    temp = 0
    # set up rounds
    round = 0
    curr_numOfUsers = 0
    curr_userSat = 100
    curr_dataLeak = 0
    curr_revenue = 10000
    

    def addRounds():
        if disableRounds:
            return

        checkIndicators()
        updateIndicatorsDisplay()
        
        globals()['round'] += 1
        numOfUsersCal()

        if round % 10 == 0:
            if staffSalary() > 0:
                globals()['revenue'] -= staffSalary()
                renpy.show_screen('deductSalary')
       
        if round==firstEvent:
            globals()['startEvent'] = firstEvent
            chooseResign()
            malwareSelection()

        for r in deliverPatch:
            if round == r:
                locatePatches(patchLocate)
        globals()['latestPatch'] = checkRound(round)

        if round % 12 == 0:
            if not checkMemberRatio():
                renpy.show_screen('RatioRecommend')

        if round > firstEvent:
            if (round - firstEvent) % malwareRound == 0:
                malwareSelection()

            if (round - firstEvent) % pwRound == 0:
                pwPolicy()
        
        for newPatch in deliverPatch:
            if round == newPatch:
                # renpy.show_screen('TransparentScreen_02')
                renpy.show_screen('EventsPopUp')
                renpy.show_screen('Lightbulb_On')
        
        if startEvent > 0:
            if ((round - startEvent) % devEventRunTime == 0):
                # renpy.show_screen('TransparentScreen_02')
                renpy.show_screen('Lightbulb_On')
                DevEventPopup(round)
                dbMsg()
                renpy.show_screen('DevEventPopUp')
                #ui.timer(1.0, Show("DevEventPopUp"))

        if round == revokeDeadline and checkRevokeCorrect() == False:
            renpy.show_screen('revokeWarning')
        
        if round >= revokeDeadline and not checkMinRoles() and displayScreens[2]['display'] == False:
            renpy.show_screen('missingRoles')

        if round % 7 == 0:
            renpy.show_screen('Lightbulb_On')
            summaryMsgSelection()

        if round == 50:
            renpy.show_screen('end_50')
        if len(publishedFeatures) == len(tables) and malwareFlag == 0 and patchFlag == 0 and dataBreachFlag == 0 and pwFlag == 0:
            renpy.show_screen('end_developed_all')
        if round >= 35 and (userSat < 10 or dataLeak >= 100 or revenue < 5000):
            renpy.show_screen('end_poor')


    # toggle screen display
    def toggleDisplay(currentScreen):
        if displayScreens[currentScreen]['display']:
            return
        displayScreens[currentScreen]['display'] = not displayScreens[currentScreen]['display']
        for screen in range (len(displayScreens)):
            if screen != currentScreen:
                displayScreens[screen]['display'] = False
            elif screen == 0 and screen == currentScreen :
                renpy.hide_screen('opening')
                renpy.hide_screen('production')
                renpy.hide_screen('team')
                renpy.show_screen('development_background')
                renpy.show_screen('shared')
                renpy.show_screen('development')
                renpy.show_screen('indicators')
                renpy.show_screen('Lightbulb_Off')
                renpy.restart_interaction()
                resetTeamRole()
            elif screen == 1 and screen == currentScreen :
                renpy.hide_screen('opening')
                renpy.hide_screen('development')
                renpy.hide_screen('team')
                renpy.show_screen('development_background')
                renpy.show_screen('shared')
                renpy.show_screen('production')
                renpy.show_screen('indicators')
                renpy.show_screen('Lightbulb_Off')
                temp_setup()
                resetTeamRole()
                renpy.restart_interaction()
            elif screen == 2 and screen == currentScreen :
                renpy.hide_screen('opening')
                renpy.hide_screen('shared')
                renpy.hide_screen('development')
                renpy.hide_screen('production')
                renpy.hide_screen('development_background')
                renpy.show_screen('team')
                renpy.show_screen('indicators')
                renpy.show_screen('Lightbulb_Off')
                renpy.restart_interaction()

    def updateIndicatorsDisplay():
        globals()['curr_numOfUsers'] = numOfUsers - curr_numOfUsers
        globals()['curr_userSat'] = userSat - curr_userSat
        globals()['curr_dataLeak'] = dataLeak - curr_dataLeak
        globals()['curr_revenue'] = revenue - curr_revenue

        renpy.show_screen('IndicatorsUpdatePerRound')

        ui.timer(3.0, Hide("IndicatorsUpdatePerRound"))
        
        ui.timer(3.0, SetVariable("curr_numOfUsers", numOfUsers))
        ui.timer(3.0, SetVariable("curr_userSat", userSat))
        ui.timer(3.0, SetVariable("curr_dataLeak", dataLeak))
        ui.timer(3.0, SetVariable("curr_revenue", revenue))



    def staffSalary():
        staffNum = 0
        for staff in toBeTeamList:
            if staff != "Team" and staff != '-':
                staffNum += 1
        return (staffNum * 500)
    
    final_score = 0
    temp_numOfUsers = 0
    temp_userSat = 0
    temp_dataLeak = 0
    temp_revenue = 0
    Achievements = []
    OverallComments = []
    def endGameSetup():
        renpy.show_screen('endAnimation')
        ui.timer(2.0, Hide("endAnimation"))
        # ui.timer(2.0, Show("endGame"))
        ui.timer(2.0, Show("endGame2"))
        ui.timer(2.0, Show("restartGameButton_dull"))
        ui.timer(5.0, Hide("restartGameButton_dull"))
        ui.timer(5.0, Show("restartGameButton"))
        

        # pwFlag, malwareFlag, patchFlag, dataBreachFlag
        if pwFlag == 1:
            Achievements.append('Team members’ password are updated, UberLock is safer now')
        else:
            OverallComments.append("Change team members' password regularly to secure the application")

        # if malwareFlag == 0 and len(publishedFeatures) > 0:
        if malwareFlag == 0:
            Achievements.append('Malwares were uninstalled and have prevent outsider attack') # cleaned the malware
        else:
            OverallComments.append("If you want to reduce the amount of records exposed, make sure you have remove malwares immediately")

        if patchFlag == 0 and len(publishedFeatures) > 0:
            Achievements.append('Multiple Patches are installed, has successfully strengthened the security level of UberLock') # deployed patches
        else:
            OverallComments.append("If you want to improve user’s satisfaction, pay attention on the new patch updates carefully. It helps to minimise rise of data leakage, so as user satisfaction")

        if dataBreachFlag == 0 and len(publishedFeatures) > 0:
            Achievements.append('Multiple data breaches are solved')
            OverallComments.append("You've really done a great job, keep it up!!!")
            
        if revokeFlag == 1:
            Achievements.append('Access permission of former team member is revoked!')
        else:
            OverallComments.append("Pay attention to the leaving of former team members, ignoring their access permission may cause unpredictable impacts")

        ISD = 0
        for db in tables:
            if db['sensitivity'] >= 30:
                ISD += 1
        if ISD < (len(tables) / 2):
            Achievements.append("You have done sufficient protection on encrypting databases! You’re Fantastic!!")
        else:
            OverallComments.append("You’ve done a lot, and great job! It seems like you have not sufficient encryption on database, try to set higher encryption strength to improve security next time!")

        if len(publishedFeatures) == len(tables):
            Achievements.append('You have release all features!')


        # # each indicator 25%
        globals()['temp_numOfUsers'] = numOfUsers / 70
        globals()['temp_userSat'] = userSat
        globals()['temp_revenue'] = revenue / 9000

        globals()['temp_numOfUsers'] = min(max(temp_numOfUsers, 1), 5)
        globals()['temp_userSat'] = min(max(temp_userSat / 20, 1), 5)
        globals()['temp_dataLeak'] = min(max(temp_dataLeak % 1000, 1), 5)
        globals()['temp_revenue'] = min(max(temp_revenue, 1), 5)

        if temp_dataLeak == 1:
            globals()['temp_dataLeak'] = 5
        elif temp_dataLeak == 2:
            globals()['temp_dataLeak'] = 4
        elif temp_dataLeak == 3:
            globals()['temp_dataLeak'] = 3
        elif temp_dataLeak == 4:
            globals()['temp_dataLeak'] = 2
        elif temp_dataLeak == 5:
            globals()['temp_dataLeak'] = 1


        globals()['final_score'] = (temp_numOfUsers + temp_userSat + temp_dataLeak + temp_revenue) / 4


    def restartGame():
        # restartRounds()
        # renpy.hide_screen('endAnimation')
        # renpy.hide_screen('endGame')
        renpy.reload_script()
        # renpy.full_restart()
        return

    def reOpenLightbulb():
        for newPatch in deliverPatch:
            if round == newPatch:
                renpy.show_screen('Lightbulb_On')
                renpy.show_screen('EventsPopUp')
                
        if startEvent > 0:
            if ((round - startEvent) % devEventRunTime == 0):
                renpy.show_screen('Lightbulb_On')
                dbMsg()
                renpy.show_screen('DevEventPopUp')

        if round % 7 == 0:
            renpy.show_screen('Lightbulb_On')
            renpy.show_screen('summaryPopUp')
                
screen IndicatorsUpdatePerRound():

    if curr_numOfUsers != numOfUsers and curr_numOfUsers > 0:
        text "{color=#66ff33} +[curr_numOfUsers] {/color}" xalign 0.36 yalign 0.002 size 12
    elif curr_numOfUsers != numOfUsers and curr_numOfUsers < 0:
        text "{color=#EE5C65} [curr_numOfUsers] {/color}" xalign 0.36 yalign 0.002 size 12

    if curr_userSat != userSat and curr_userSat > 0:
        text "{color=#66ff33} +[curr_userSat] {/color}" xalign 0.48 yalign 0.002 size 12
    elif curr_userSat != userSat and curr_userSat < 0:
        text "{color=#EE5C65} [curr_userSat] {/color}" xalign 0.48 yalign 0.002 size 12

    if curr_dataLeak != dataLeak and curr_dataLeak > 0:
        text "{color=#EE5C65} +[curr_dataLeak] {/color}" xalign 0.6 yalign 0.002 size 12
    elif curr_dataLeak != dataLeak and curr_dataLeak < 0:
        text "{color=#66ff33} [curr_dataLeak] {/color}" xalign 0.6 yalign 0.002 size 12

    if curr_revenue != revenue and curr_revenue > 0:
        text "{color=#66ff33} +[curr_revenue] {/color}" xalign 0.75 yalign 0.002 size 12
    elif curr_revenue != revenue and curr_revenue < 0:
        text "{color=#EE5C65} [curr_revenue] {/color}" xalign 0.75 yalign 0.002 size 12



screen background():
    # Top
    add im.Scale("UI_tab_bkg.png", 1300, 730) xalign 0.5 yalign 0.3

    add im.Scale("UI_top.png", 1300, 50) xalign 0.01
    text "{color=#ffffff} UberLock {/color}" xalign 0.01 yalign 0.01 size 30
    # imagebutton idle im.Scale("end_Game.png", 80, 30) xalign 0.2 yalign 0.01 action Function(endGameSetup)

    text "{color=#ffffff} Round: [round] {/color}" xalign 0.9 yalign 0.01 size 28
    imagebutton idle im.Scale("next_idle.png", 50, 30) hover im.Scale("next_hover.png", 50, 30) xalign 0.95 yalign 0.01 action Function(addRounds)
    imagebutton idle im.Scale("restart_idle.png", 30, 30) hover im.Scale("restart_hover.png", 30, 30) xalign 0.99 yalign 0.01 action Function(restartGame)

    # text "{color=#6495ED} malwareFlag: [malwareFlag] {/color}" xalign 0.2 yalign 0.01
    # text "{color=#6495ED} pwFlag: [pwFlag] {/color}" xalign 0.4 yalign 0.01
    # text "{color=#6495ED} patchFlag: [patchFlag] {/color}" xalign 0.2 yalign 0.05
    # text "{color=#6495ED} dataBreachFlag: [dataBreachFlag] {/color}" xalign 0.4 yalign 0.05
    # text "{color=#6495ED} installedDB: [installedDB] {/color}" xalign 0.6 yalign 0.01

    # navigation tabs
    hbox:
        xalign 0.01
        yalign 0.08
        for i in range (len(displayScreens)):
            if displayScreens[i]['display'] == True:
                imagebutton idle im.Scale("UI_tab_light.png", 200, 50) action Function(toggleDisplay, i)
            else:
                imagebutton idle im.Scale("UI_tab_dark.png", 200, 50) action Function(toggleDisplay, i)
    add im.Scale("UI_tab_line.png", 1300, 6) xalign 0.5 yalign 0.132

    hbox:
        xalign 0.04
        yalign 0.09
        spacing 70
        for i in displayScreens:
            text "{color=#000000} [i[name]] {/color}" size 20

    if pwFlag == 1 or malwareFlag == 1 or patchFlag == 1:
        add im.Scale("alert_red_dot.png", 30, 30) xalign 0.295 yalign 0.09

    if pwFlag == 1 and hackedMember != '':
        for j in range(len(members)):
            if members[j]['name'] == hackedMember:
                if teamList[j] == 'Development':
                    add im.Scale("alert_red_dot.png", 30, 30) xalign 0.135 yalign 0.09

screen development_background():
    add im.Scale("UI_screen_dev_prod.png", 1285, 620) xalign 0 yalign 0.99


screen opening():
    add im.Scale("opening_bkg.png", 1285, 722) xalign 0 yalign 0.99
    imagebutton idle im.Scale("opening_start.png", 593, 124) hover im.Scale("opening_start_hover.png", 593, 124) xalign 0.5 yalign 0.7 action [Hide('opening'), Show('background'), Function(toggleDisplay, 0)]


screen endAnimation():
    imagebutton idle im.Scale("end_game_page.png", 1300, 780) xalign 0.5 yalign 0.3 action NullAction()

screen endGame():
    add im.Scale("panel_grey.png", 1300, 700) xalign 0.5 yalign 0.3
    add im.Scale("end_game_score.png", 1750, 980) zoom 0.8 xalign 0.5 yalign 0.3
    
    add im.Scale("UI_tab_line.png", 400, 55) xalign 0.5 yalign 0.38
    hbox:
        xalign 0.5 yalign 0.38
        spacing 30
        add im.Scale("indicator_user.png", 25, 25) ypos -30
        text "{color=#d20000} [numOfUsers] {/color}" size 14 ypos -23
        add im.Scale("indicator_satisfaction.png", 25, 25) ypos -30
        text "{color=#d20000} [userSat] {/color}" size 14 ypos -23
        add im.Scale("indicator_leakage.png", 25, 25) ypos -30
        text "{color=#d20000} [dataLeak] {/color}" size 14 ypos -23
        add im.Scale("indicator_revenue.png", 25, 25) ypos -30
        text "{color=#d20000} [revenue] {/color}" size 14 ypos -23

    text "{color=#203864}  %s {/color}"%final_score xalign 0.6 yalign 0.41 size 30
    vbox:
        xalign 0.5
        yalign 0.75
        for task in Achievements[:4]:
            vbox:
                xalign 0
                text "{color=#203864} • %s {/color}"%task xalign 0.5 yalign 0.5 size 14 text_align 0    
    imagebutton idle im.Scale("end_game_restart.png", 700, 90) xpos 280 ypos 584 action Function(restartGame)

screen end_50():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 700, 583) xalign 0.5 yalign 0.5
    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.73 yalign 0.13 action Hide('end_50')

    text "{color=#000000} {b} Game Ended {/b} {/color}" xalign 0.51 yalign 0.2 size 20
    text "{color=#000000} Congratulations! You've completed 50 rounds,  {/color}" xalign 0.5 yalign 0.47 size 18
    text "{color=#000000} and have come to the end of our Game! {/color}" xalign 0.5 yalign 0.51 size 18
    
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.82 action [Hide('end_50'), Function(endGameSetup)]
    text "{color=#ffffff} {b}Next{/b} {/color}" xalign 0.5 yalign 0.8 size 14


screen end_developed_all():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 700, 583) xalign 0.5 yalign 0.5
    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.73 yalign 0.13 action Hide('end_developed_all')

    text "{color=#000000} {b} Game Ended {/b} {/color}" xalign 0.51 yalign 0.2 size 20
    text "{color=#000000} Congratulations! You've developed and released ALL features!  {/color}" xalign 0.5 yalign 0.47 size 18
    text "{color=#000000} And have come to the end of our Game! {/color}" xalign 0.5 yalign 0.51 size 18
    
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.82 action [Hide('end_developed_all'), Function(endGameSetup)]
    text "{color=#ffffff} {b}Next{/b} {/color}" xalign 0.5 yalign 0.8 size 14


screen end_poor():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 700, 583) xalign 0.5 yalign 0.5
    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.73 yalign 0.13 action Hide('end_poor')

    text "{color=#000000} {b} Game Ended {/b} {/color}" xalign 0.51 yalign 0.2 size 20
    text "{color=#000000} Knowing that you've a lot of hard work,  {/color}" xalign 0.5 yalign 0.47 size 18
    text "{color=#000000} but it has come to the end of our Game! {/color}" xalign 0.5 yalign 0.51 size 18
    
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.82 action [Hide('end_poor'), Function(endGameSetup)]
    text "{color=#ffffff} {b}Next{/b} {/color}" xalign 0.5 yalign 0.8 size 14

screen restartGameButton():
    imagebutton idle im.Scale("restart_idle.png", 30, 30) xalign 0.99 yalign 0.01 action Function(restartGame)

screen restartGameButton_dull():
    imagebutton idle im.Scale("restart_hover.png", 30, 30) xalign 0.99 yalign 0.01 action NullAction()


screen endGame2():
    add im.Scale("UI_screen_endGame.png", 1300, 720) xalign 0.5 yalign 0.3
    text "{color=#000000} Completed Rounds: [round] {/color}" xalign 0.95 yalign 0.01 size 13

    text "{color=#000000} {b} Completed Features: {/b} {/color}" xalign 0.06 yalign 0.17 size 20
    vbox:
        xalign 0.06 yalign 0.25
        xanchor 0 yanchor 0
        spacing 10
        hbox:
            spacing 20
            add im.Scale("UI_symbol_tick.png", 50, 50) ypos - 10
            text "{color=#000000} Developed:{/color}" size 20
            text "{color=#000000} %s out of 9 Features {/color}"%(len(developingDbs) + len(publishedFeatures)) size 20
        hbox:
            spacing 20
            add im.Scale("UI_symbol_tick.png", 50, 50) ypos - 10
            text "{color=#000000} Release:{/color}" size 20
            text "{color=#000000} %d out of 9 Features {/color}"%len(publishedFeatures) size 20 xpos 30




    text "{color=#000000} {b} Excellent Achievements: {/b} {/color}" xalign 0.06 yalign 0.5 size 20
    vbox:
        xalign 0.06 yalign 0.55
        xanchor 0 yanchor 0
        spacing 10        
        for task in Achievements:
            hbox:
                spacing 20
                xmaximum 500
                add im.Scale("UI_symbol_star.png", 50, 50)
                text "{color=#203864}%s {/color}"%task xalign 0.5 yalign 0.5 size 20  




    text "{color=#000000} {b} Scores: {/b} {/color}" xalign 0.55 yalign 0.17 size 20
    vbox:
        xalign 0.555 yalign 0.24
        xanchor 0 yanchor 0
        spacing 34
        text "{color=#000000} Number of Users {/color}" size 15
        text "{color=#000000} User Satisfaction {/color}" size 15
        text "{color=#000000} Number of Records Exposed {/color}" size 15
        text "{color=#000000} Budget {/color}" size 15
        text "{color=#000000} {b}Total Score{/b} {/color}" size 15 xpos 50 ypos 10
    vbox:
        xalign 0.751 yalign 0.232
        xanchor 0 yanchor 0
        spacing 23
        hbox:
            for i in range(temp_numOfUsers):
                add im.Scale("UI_symbol_score.png", 27, 27)
        hbox:
            for i in range(temp_userSat):
                add im.Scale("UI_symbol_score.png", 27, 27)
        hbox:
            xpos 1 ypos 2
            for i in range(temp_dataLeak):
                add im.Scale("UI_symbol_score.png", 27, 27)
        hbox:
            xpos 1 ypos 3
            for i in range(temp_revenue):
                add im.Scale("UI_symbol_score.png", 27, 27)
        hbox:
            xpos 50 ypos 18
            for i in range(final_score):
                add im.Scale("UI_symbol_score_total.png", 27, 27)
    vbox:
        xalign 0.88 yalign 0.24
        xanchor 0 yanchor 0
        spacing 38
        text "{color=#000000} [numOfUsers] {/color}" size 12
        text "{color=#000000} [userSat]/ 100 {/color}" size 12 xpos -20
        text "{color=#000000} [dataLeak] {/color}" size 12
        text "{color=#000000} [revenue] {/color}" size 12 xpos -14


    text "{color=#000000} {b} Overall Comments: {/b} {/color}" xalign 0.63 yalign 0.65 size 20
    vbox:
        xalign 0.52 yalign 0.68
        xanchor 0 yanchor 0
        spacing 10
        for comment in OverallComments[:2]:
            hbox:
                spacing 20
                xmaximum 500
                if comment == "You've really done a great job, keep it up!!!":
                    add im.Scale("UI_symbol_good.png", 50, 50)
                else:
                    add im.Scale("UI_symbol_addoil.png", 50, 50)
                text "{color=#203864}%s {/color}"%comment xalign 0.5 yalign 0.5 size 20  



label start:

    # call screen background
    # show screen background
    call screen opening
    show screen opening

    return
