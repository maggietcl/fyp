init python:
    
    startToDo_str = ""
    startProgress_str = ""
    startDone_str = ""

    toDoList = ['Cab', 'Rider Bookmarks', 'Location', 'Map Grid', 'Assurance','Trip', 'Rider', 'Driver', 'Payment']
    inProgress = []
    done = []

    displayDBs = []

    def startDevelop(i):
        globals()['startToDo_str'] = toDoList[i]
        renpy.show_screen('toDoPopWindow')

    def updateToDoArray():
        if checkMinRoles():
            inProgress.append(startToDo_str)
            toDoList.remove(startToDo_str)

            renpy.hide_screen('toDoPopWindow')
            globals()['startToDo_str'] = ""
            addRounds()
        else:
            toggleDisplay(2)
            renpy.hide_screen('toDoPopWindow')
            renpy.show_screen('selectTeamPriority')

            
    def startInProgress(i):
        globals()['startProgress_str'] = inProgress[i]
        renpy.show_screen('inProgressPopWindow')

    def buildTable():
        renpy.hide_screen('inProgressPopWindow')
        if startProgress_str not in developingDbs:
            displayDBs.append(startProgress_str)
            developingDbs.append(startProgress_str)
            addRounds()

    def startDone(feature):
        if startProgress_str not in developingDbs:
            buildTable()
        globals()['startDone_str'] = feature
        globals()['startProgress_str'] = ""
        done.append(startDone_str)
        inProgress.remove(startDone_str)
        renpy.hide_screen('inProgressPopWindow')
        addRounds()

    def doneConfirm(feature):
        globals()['startDone_str'] = feature
        renpy.show_screen('donePopWindow')

    deployedEssentials = []
    def doneTask():
        deployedFeatures.append(startDone_str)
        done.remove(startDone_str)
        renpy.hide_screen('donePopWindow')        
        addRounds()
        manpowerCal()
        for i in range(len(EssentialFeatures)):
            if startDone_str == EssentialFeatures[i]:
                deployedEssentials.append(startDone_str)
                return
        globals()['startDone_str'] = ""

    def startnDebugFeature(feature):
        if search(feature, testnDebug, "name") == []:
            return False
        else:
            renpy.show_screen('ConfirmPatchTesting')
            return True

    def search(item, Arr, key):
        return [element for element in Arr if element[0][key] == item]

    def findIndex(item, Arr, key):
        return [i for i, d in enumerate(Arr) if d[key] == item]


screen development():
    tag development
    
    text "{color=#000000} {u} To - Do {/u} {/color}" xalign 0.04 yalign 0.24 size 20
    text "{color=#000000} {u} In Progress {/u} {/color}" xalign 0.22 yalign 0.24 size 20
    text "{color=#000000} {u} Done {/u} {/color}" xalign 0.41 yalign 0.24 size 20


    # To Do Post-it
    vbox:
        xalign 0.01
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(toDoList))[::2]:
            hbox:
                if toDoList[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startDevelop, i)
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action Function(startDevelop, i)
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%toDoList[i] size 9 xpos -111 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startDevelop, i)
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action Function(startDevelop, i)
                    text "{color=#000000} %s {/color}"%toDoList[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2
    vbox:
        xalign 0.08
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(toDoList))[1::2]:
            hbox:
                if toDoList[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startDevelop, i)
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action Function(startDevelop, i)
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%toDoList[i] size 9 xpos -111 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startDevelop, i)
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action Function(startDevelop, i)
                    text "{color=#000000} %s {/color}"%toDoList[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2

    
    # In Progress Post-it
    vbox:
        xalign 0.18
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(inProgress))[::2]:
            hbox:
                if inProgress[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startInProgress, i)
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action Function(startInProgress, i)                    
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%inProgress[i] size 9 xpos -101 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startInProgress, i)
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action Function(startInProgress, i)   
                    text "{color=#000000} %s {/color}"%inProgress[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2
    vbox:
        xalign 0.25
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(inProgress))[1::2]:
            hbox:
                if inProgress[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startInProgress, i)
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action Function(startInProgress, i)  
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%inProgress[i] size 9 xpos -101 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(startInProgress, i)
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action Function(startInProgress, i)   
                    text "{color=#000000} %s {/color}"%inProgress[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2
            
    
    # Done Post-it
    vbox:
        xalign 0.35
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(done))[::2]:
            hbox:
                if done[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(doneConfirm, done[i])
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action Function(doneConfirm, done[i])                    
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%done[i] size 9 xpos -115 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(doneConfirm, done[i])
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action Function(doneConfirm, done[i])  
                    text "{color=#000000} %s {/color}"%done[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2
    vbox:
        xalign 0.43
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(done))[1::2]:
            hbox:
                if done[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(doneConfirm, done[i])
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action Function(doneConfirm, done[i])   
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%done[i] size 9 xpos -115 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action Function(doneConfirm, done[i])
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action Function(doneConfirm, done[i])  
                    text "{color=#000000} %s {/color}"%done[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2
             



screen toDoPopWindow():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 283) xalign 0.5 yalign 0.5
    
    text "{color=#000000} {b} Develop Feature {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    text "{color=#000000} Start to develop -  [startToDo_str]? {/color}" xalign 0.5 yalign 0.47 size 18
    
    imagebutton idle im.Scale("UI_popup_cancel.png", 200, 55) hover im.Scale("UI_popup_cancel_hover.png", 200, 55) xalign 0.4 yalign 0.59 action Hide('toDoPopWindow')
    text "{color=#000000} Cancel {/color}" xalign 0.41 yalign 0.58 size 15
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) hover im.Scale("UI_popup_okay_hover.png", 200, 55) xalign 0.6 yalign 0.59 action Function(updateToDoArray)
    text "{color=#ffffff} {b} Start Develop {/b} {/color}" xalign 0.595 yalign 0.58 size 15

    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.35 action Hide('toDoPopWindow')

screen inProgressPopWindow():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 400) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} [startProgress_str] {/b} {/color}" xalign 0.5 yalign 0.32 size 20
    
    if startProgress_str in developingDbs:
        imagebutton idle im.Scale("UI_popup_disable.png", 200, 55) xalign 0.5 yalign 0.45 action NullAction()
        text "{color=#ffffff} Create Database {/color}" xalign 0.5 yalign 0.45 size 15
        text "{color=#3366FF} • Please select\n encryption strength for\n each database attribute {/color}" xalign 0.65 yalign 0.45 size 10
    else:
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.45 action Function(buildTable)
        text "{color=#ffffff} Create Database {/color}" xalign 0.5 yalign 0.45 size 15
    
    if search(startProgress_str, testnDebug, "name"):
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.55 action [Show('ConfirmPatch'), SetVariable("DBpopUp_str", startProgress_str), Hide('inProgressPopWindow')]
        text "{color=#ffffff} Start Patch {/color}" xalign 0.5 yalign 0.55 size 15
    else:
        imagebutton idle im.Scale("UI_popup_disable.png", 200, 55) xalign 0.5 yalign 0.55 action NullAction()
        text "{color=#ffffff} Patch {/color}" xalign 0.5 yalign 0.545 size 15

    if tables[int(findIndex(startProgress_str, tables, "name")[0])]['sensitivity'] == "50" or not ISD(startProgress_str):
        imagebutton idle im.Scale("UI_popup_disable.png", 200, 55) xalign 0.5 yalign 0.65 action NullAction()
    else:
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.65 action Function(startDone, startProgress_str)
    text "{color=#ffffff} Complete Task {/color}" xalign 0.5 yalign 0.642 size 15

    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.27 action Hide('inProgressPopWindow')

screen donePopWindow():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 283) xalign 0.5 yalign 0.5
    
    text "{color=#000000} {b} Deploy Feature {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    text "{color=#000000} Deploy Feature -  [startDone_str]? {/color}" xalign 0.5 yalign 0.47 size 18
    
    imagebutton idle im.Scale("UI_popup_cancel.png", 200, 55) hover im.Scale("UI_popup_cancel_hover.png", 200, 55) xalign 0.4 yalign 0.59 action Hide('donePopWindow')
    text "{color=#000000} Cancel {/color}" xalign 0.41 yalign 0.58 size 15
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) hover im.Scale("UI_popup_okay_hover.png", 200, 55) xalign 0.6 yalign 0.59 action Function(doneTask)
    text "{color=#ffffff} {b} Deploy Feature {/b} {/color}" xalign 0.595 yalign 0.58 size 15

    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.35 action Hide('toDoPopWindow')