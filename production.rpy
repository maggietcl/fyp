init python:

    deployedFeatures = []
    publishedFeatures = []
    servers = ["Log server","Centralized audit server"]

    startDeployed_str = ""
    startProgress_str = ""
    startDone_str = ""

    publishedDbs = []
    testnDebug = []

    def startDeployed(i):
        globals()['startDeployed_str'] = deployedFeatures[i]
        renpy.show_screen('deployedPopWindow')

    def updateToPublished():
        developingDbs.remove(startDeployed_str)
        publishedFeatures.append(startDeployed_str)
        publishedDbs.append([
            {'name': startDeployed_str, 'patch': []}
        ])
        for patch in patches:
            temp_list = []
            temp_list.append({'id': patch['id']})
            temp_list.append({'patch-status': patch['patch-status']})
            publishedDbs[-1][0]['patch'].append(temp_list)
        for feature in range(len(publishedFeatures)):
            deployedFeatures.clear()
        addRounds()        
        renpy.hide_screen('deployedPopWindow')
        

        renpy.hide_screen('deployedPopWindow')
        globals()['startDeployed_str'] = ""
        addRounds()

    # def PublishedFeatures():
    #     publishedFeatures.remove(startProgress_str)
    #     renpy.hide_screen('publishedFeaturesPopWindow')
    #     globals()['startProgress_str'] = ""
    #     addRounds()

    def startPublishAllFeatures():
        renpy.show_screen('publishAllFeatures')

    temp_list = []
    def confirmPublishAllFeatures():
        if len(deployedEssentials) >= len(EssentialFeatures):
            for feature in range(len(deployedFeatures)):
                developingDbs.remove(deployedFeatures[feature])

                publishedFeatures.append(deployedFeatures[feature])
                publishedDbs.append([
                    {'name': deployedFeatures[feature], 'patch': [], 'malware': 'clean'}
                ])
            for feature in range(len(publishedFeatures)):
                deployedFeatures.clear()
            addRounds()
            patchSetup(round)
        renpy.hide_screen('publishAllFeatures')

    # need to remove this later
    set_up_deployed = 0
    def temp_setup():
        return
        globals()['teamList'] = ['Development','Development','Production','Team','Team','Team','Team','Production']
        globals()['roleList'] = ['DBA','Developer','DBA','Role','Role','Role','Role','Developer']
        if set_up_deployed > 0:
            return

        globals()['displayDBs'] = ['Trip', 'Payment']
        globals()['developingDbs'] = ['Trip', 'Payment']
        globals()['toDoList'] = ['Cab', 'Rider Bookmarks', 'Location', 'Map Grid', 'Assurance', 'Rider', 'Driver']

        globals()['deployedFeatures'] = ['Trip', 'Payment']
        globals()['deployedEssentials'] = ['Trip', 'Payment']
    
        globals()['set_up_deployed'] += 1


screen production():
    # text "{color=#3399ff} Production Tab {/color}" xalign 0.5 yalign 0.5 size 40

    text "{color=#000000} {u} Deployed Features {/u} {/color}" xalign 0 yalign 0.24 size 18
    text "{color=#000000} {u} Released Features {/u} {/color}" xalign 0.21 yalign 0.24 size 18
    text "{color=#000000} {u} Servers {/u} {/color}" xalign 0.41 yalign 0.24 size 20


    # Deployed Features Post-it
    vbox:
        xalign 0.01
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(deployedFeatures))[::2]:
            hbox:
                if deployedFeatures[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action NullAction()
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%deployedFeatures[i] size 9 xpos -111 ypos 20 text_align 0.5 line_spacing 2
                else:
                    imagebutton idle im.Scale("post_green.png", 80, 80) action NullAction()
                    text "{color=#000000} %s {/color}"%deployedFeatures[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2
    vbox:
        xalign 0.08
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(deployedFeatures))[1::2]:
            hbox:
                if deployedFeatures[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action NullAction()
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%deployedFeatures[i] size 9 xpos -111 ypos 20 text_align 0.5 line_spacing 2
                else:
                    imagebutton idle im.Scale("post_green.png", 80, 80) action NullAction()
                    text "{color=#000000} %s {/color}"%deployedFeatures[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2

    # Deploy Features
    if deployedFeatures:
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) hover im.Scale("UI_popup_okay_hover.png", 200, 55) xalign 0.01 yalign 0.99 action Function(startPublishAllFeatures)
        text "{color=#ffffff} {b} Release All Features {/b} {/color}" xalign 0.025 yalign 0.96 size 12
    else:
        imagebutton idle im.Scale("UI_popup_disable.png", 200, 55) xalign 0.01 yalign 0.99 action NullAction()
        text "{color=#ffffff} {b} Release All Features {/b} {/color}" xalign 0.025 yalign 0.96 size 12


    # Published Features Post-it
    vbox:
        xalign 0.18
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(publishedFeatures))[::2]:
            hbox:
                if publishedFeatures[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action NullAction()
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%publishedFeatures[i] size 9 xpos -111 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action NullAction()
                    text "{color=#000000} %s {/color}"%publishedFeatures[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2
    vbox:
        xalign 0.25
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(publishedFeatures))[1::2]:
            hbox:
                if publishedFeatures[i] in EssentialFeatures:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("post_green_essential.png", 80, 80) action NullAction()
                    text "{color=#000000} Essential {/color}" size 9 xpos -72 ypos 58
                    text "{color=#000000} %s {/color}"%publishedFeatures[i] size 9 xpos -101 ypos 20 text_align 0.5 line_spacing 2
                else:
                    if disableRounds:
                        imagebutton idle im.Scale("post_grey.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("post_green.png", 80, 80) action NullAction()
                    text "{color=#000000} %s {/color}"%publishedFeatures[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2

    # Servers
    vbox:
        xalign 0.35
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(servers))[::2]:
            hbox:
                if servers[i]=="Log server":
                    if pwFlag == 0:
                        imagebutton idle im.Scale("server.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("server_alert.png", 80, 80) action Show('logReportPopup')
                elif servers[i]=="Centralized audit server":
                    if malwareFlag == 0:
                        imagebutton idle im.Scale("server.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("server_alert.png", 80, 80) action Show('auditReportPopup')
                else:
                    imagebutton idle im.Scale("server.png", 80, 80) action NullAction()
                #imagebutton idle im.Scale("server.png", 80, 80) action If ('servers[i]=="Log server"',true=Show('logReportPopup'),false=Show('auditReportPopup') if servers[i]=="Centralized audit server" else NullAction())
                text "{color=#000000} %s {/color}"%servers[i] size 9 xpos -72 ypos 20 text_align 0.5 line_spacing 2
    vbox:
        xalign 0.42
        yalign 0.275
        xanchor 0
        yanchor 0
        xmaximum 100
        for i in range (len(servers))[1::2]:
            hbox:
                if servers[i]=="Log server":
                    if pwFlag == 0:
                        imagebutton idle im.Scale("server.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("server_alert.png", 80, 80) action Show('logReportPopup')
                elif servers[i]=="Centralized audit server":
                    if malwareFlag == 0:
                        imagebutton idle im.Scale("server.png", 80, 80) action NullAction()
                    else:
                        imagebutton idle im.Scale("server_alert.png", 80, 80) action Show('auditReportPopup')
                else:
                    imagebutton idle im.Scale("server.png", 80, 80) action NullAction()
                #imagebutton idle im.Scale("server.png", 80, 80) action If ('servers[i]=="Log server"',true=Show('logReportPopup'),false=Show('auditReportPopup') if servers[i]=="Centralized audit server" else NullAction())
                text "{color=#000000} %s {/color}"%servers[i] size 9 xpos -71 ypos 20 text_align 0.5 line_spacing 2


screen deployedPopWindow():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 283) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Release Feature {/b} {/color}" xalign 0.51 yalign 0.4 size 20    
    text "{color=#000000} Start to deploy -  [startDeployed_str]? {/color}" xalign 0.5 yalign 0.47 size 20

    imagebutton idle im.Scale("UI_popup_cancel.png", 200, 55) xalign 0.4 yalign 0.59 action Hide('deployedPopWindow')
    text "{color=#000000} Cancel {/color}" xalign 0.41 yalign 0.58 size 15
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.6 yalign 0.59 action Function(updateToPublished)
    text "{color=#ffffff} {b} Release {/b} {/color}" xalign 0.595 yalign 0.58 size 15

    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.35 action Hide('deployedPopWindow')

screen publishAllFeatures():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 283) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Release All Features {/b} {/color}" xalign 0.51 yalign 0.38 size 20

    # Messages
    if len(deployedEssentials) == 0:
        text "{color=#000000} You have to deploy all essential features first! {/color}" xalign 0.5 yalign 0.47 size 18 text_align 0.5
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action Hide('publishAllFeatures')
        text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15
    elif len(deployedEssentials) < len(EssentialFeatures):
        text "{color=#000000} Some Essential Features are missing, \nyou have to complete them first {/color}" xalign 0.5 yalign 0.47 size 18 text_align 0.5
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action Hide('publishAllFeatures')
        text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15
    elif len(deployedEssentials) == len(EssentialFeatures):
        text "{color=#000000} Do you want to release all features? {/color}" xalign 0.5 yalign 0.47 size 18
        imagebutton idle im.Scale("UI_popup_cancel.png", 200, 55) xalign 0.4 yalign 0.59 action Hide('publishAllFeatures')
        text "{color=#000000} Cancel {/color}" xalign 0.41 yalign 0.58 size 15
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.6 yalign 0.59 action Function(confirmPublishAllFeatures)
        text "{color=#ffffff} {b} Okay {/b} {/color}" xalign 0.595 yalign 0.58 size 15

    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.35 action Hide('publishAllFeatures')
