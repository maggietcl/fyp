init python:
    import random
    from datetime import datetime
    from datetime import timedelta
    events = [
        {
            'type': 'devEvent',
            'name': 'Resign of team member',
            'image': 'event_resign_1.jpeg',
            'content': 'Tommy leaves the team today. Another team member, Rebecca will take over his Former duties.',
        },
        {
            'type': 'devEvent',
            'name': 'Data breach incident',
            'image': 'data_breach_1.jpg',
            'content': 'Many customers report that their travelling history and personal data are exposed on the internet. They are worried that their privacy will be invaded',
        },
        {
            'type': 'reward',
            'name': 'Reward',
            'image': 'reward.jpg',
            'content': 'Here is a reward for your excellent performance!',
        },
        {
            'type': 'patch',
            'name': 'New Patch is Needed',
            'image': 'secure_shield.png',
            'content': 'A new patch is released to secure database. \nPlease install the new patch to secure database(s).',
        }
    ]


    # Test the patch, start patch, patching, patch completed
    patches = [
        {
            'name': '',
            'id': 'P01',
            'impacts': 'Payment, Trip, Driver',
            'details': [
                {'Type': 'Database Server'},
                {'Criticality': 'Severe'},
                {'Expected-Testing-Time': '1'},
                {'Deployment-Time': '1'},
                {'Patch-Installation-Time': '1'},
            ],
            'patch-status': 'Test the patch',
            'image': 'color_tommy.png',
            'color': 'critical'
        },
        {
            'name': '',
            'id': 'P02',
            'impacts': 'Payment, Trip, Driver',
            'details': [
                {'Type': 'Database Server'},
                {'Criticality': 'Severe'},
                {'Expected-Testing-Time': '1'},
                {'Deployment-Time': '1'},
                {'Patch-Installation-Time': '1'},
            ],
            'patch-status': 'Test the patch',
            'image': 'color_peter.png',
            'color': 'medium'
        }
    ]
    color_critical = 'EE5C65'
    color_medium = 'FFC000'
    color_least = '92D050'

    currentEvent = events[3]

    essentialsPublished = 0
    deliverPatch = []
    globals()['disableRounds'] = False

    startEvent = 0
    firstEvent = 15
    patchLocate = 0
    def patchSetup(round):
        globals()['essentialsPublished'] = round
        for i in range(2): # assume there are only 2 patches in total
            deliverPatch.append(round + 2)
            round += 2

    def locatePatches(patch):
        for i in range(len(publishedDbs)):
            checkDbPatches(publishedDbs[i])

        for i in range(len(testnDebug)):
            checkDbPatches(testnDebug[i])
        globals()['patchLocate'] += 1


    def updatePatchesInDb(database, missingPatch):
        for i in database[0]['patch']:
            if patches[missingPatch]['id'] == i[0]['id']: return

        temp_list = []
        temp_list.append({'id': patches[missingPatch]['id']})
        temp_list.append({'patch-status': patches[missingPatch]['patch-status']})
        database[0]['patch'].append(temp_list)

    latestPatch = 0
    # --> check missing patch in publishedDbs
    def checkDbPatches(database):
        globals()['latestPatch'] = checkRound(round)
        if latestPatch == 0 : return
        
        temp_check_list = []
        for patch in database[0]['patch']:
            temp_check_list.append(patch[0]['id'])

        for i in range(len(patches)):
            if i < latestPatch and patches[i]['id'] not in temp_check_list:
                updatePatchesInDb(database, i)

    def checkRound(r):
        latest = 0
        for deliver in range(len(deliverPatch)):
            if round >= deliverPatch[deliver]:
                latest += 1
        return latest


    def displayPatchPopup():
        renpy.show_screen('EventsPopUp')

    patchDetail = 0

    def showPatchDetails(patch):  #patch: numer
        globals()['patchDetail'] = patch
        renpy.show_screen('PatchDetails')

    # status: Test the patch, start patch, patching, patch completed
    def patchStatus(patch):
        globals()['patchDetail'] = patch

        if publishedDbs[DBpopUp_int][0]['patch'][patch][1]['patch-status'] == 'Test the patch':
            renpy.show_screen('ConfirmPatchTesting')
    
    
    def patchRoundCount(roundsRemaing):
        # Future:  use countdown time to measure/ experience no. of rounds remaining
        # Now: skip to rounds time already
        for rounds in range(roundsRemaing):
            addRounds()
        globals()['disableRounds'] = True
        skipPatchRounds()

    loadingAction = ''
    def skipPatchRounds():
        ui.timer(3.0, SetVariable("disableRounds", False))
        renpy.show_screen('LoadingScreen')
        ui.timer(3.0, Hide("LoadingScreen"))
        ui.timer(3.0, SetVariable("loadingAction", ''))


    def startPatchTest():
        toggleDisplay(0)

        inProgress.append(DBpopUp_str)
        publishedFeatures.remove(DBpopUp_str)

        testnDebug.append(publishedDbs[DBpopUp_int])

        publishedDbs.remove(publishedDbs[DBpopUp_int])
        developingDbs.append(DBpopUp_str)

        patchRoundCount(int(patches[patchDetail]['details'][2]['Expected-Testing-Time']))
        globals()['loadingAction'] = "Testing Patch: %s" % str(patches[patchDetail]['id'])

        ui.timer(3.0, Show("ConfirmPatch"))

        renpy.hide_screen('ConfirmPatchTesting')
        renpy.hide_screen('DBDoPopWindow')
        renpy.hide_screen('inProgressPopWindow')

    def startpatch():
        toggleDisplay(1)
        inProgress.remove(DBpopUp_str)
        developingDbs.remove(DBpopUp_str)
        publishedFeatures.append(DBpopUp_str)

        publishedDbs.append([
            {'name': DBpopUp_str, 'patch': [], 'malware': 'clean'}
        ])
        for patch in patches:
            temp_list = []
            temp_list.append({'id': patch['id']})
            temp_list.append({'patch-status': patch['patch-status']})
            publishedDbs[-1][0]['patch'].append(temp_list)

        current = 0
        for i in range(len(testnDebug)):
            if testnDebug[i][0]['name'] == DBpopUp_str:
                current = i - 1
        checkDbPatches(testnDebug[current])
        checkDbPatches(publishedDbs[-1])
        
        for i in range(len(testnDebug[current][0]['patch'])):
            if publishedDbs[-1][0]['patch'][i][1]['patch-status'] != testnDebug[current][0]['patch'][i][1]['patch-status']:
                publishedDbs[-1][0]['patch'][i][1]['patch-status'] = testnDebug[current][0]['patch'][i][1]['patch-status']
            # if publishedDbs[-1][0]['malware'] != testnDebug[current][0]['malware']:
            #     publishedDbs[-1][0]['malware'] = testnDebug[current][0]['malware']

        for j in testnDebug:
            if j[0]['name'] == publishedDbs[-1][0]['name']:
                publishedDbs[-1][0]['patch'][patchDetail][1]['patch-status'] = 'patch completed'

        patchRoundCount(int(patches[patchDetail]['details'][3]['Deployment-Time']))
        globals()['loadingAction'] = "Installing Patch: %s" % str(patches[patchDetail]['id'])

        renpy.hide_screen('ConfirmPatch')
        for db in testnDebug:
            if db[0]['name'] == DBpopUp_str:
                testnDebug.remove(db)

        checkDataBreachFactors()

    summaryMsg = ''
    targetNumOfFeatures = 0
    def summaryMsgSelection():
        globals()['summaryMsg'] = ""
        if len(publishedFeatures) == 2:
            globals()['summaryMsg'] = summaryMsgList[1]
        elif len(publishedFeatures) > 2:
            globals()['targetNumOfFeatures'] = round/50*9
            if len(publishedFeatures) < targetNumOfFeatures:
                if userSat < 40:
                    globals()['summaryMsg'] = summaryMsgList[4]
                elif dataLeak > 60:
                    globals()['summaryMsg'] = summaryMsgList[5]
                else:
                    globals()['summaryMsg'] = summaryMsgList[2]
            else:
                globals()['summaryMsg'] = summaryMsgList[3]
        else:
            globals()['summaryMsg'] = summaryMsgList[0]
        renpy.show_screen('summaryPopUp')




    tempEvent = []
    currentDevEvent = 0
    devEventStartRound = 0
    devEventRunTime = 5
    rewardRound = 25
    malwareRound = 15
    pwRound = 20
    def DevEventPopup(r):

        # if r == firstEvent:
        #     malwareSelection()

        if r > firstEvent:
            if (r - firstEvent) % rewardRound == 0:
                globals()['currentDevEvent'] = 2
            else:
                checkDataBreachFactors()
                # if dataBreachFlag == 0:
                #     globals()['currentDevEvent'] = 2
                # else:
                #     globals()['currentDevEvent'] = 1
            
            # if (r - firstEvent) % malwareRound == 0:
            #     malwareSelection()

            # if (r - firstEvent) % pwRound == 0:
            #     pwPolicy()
        
        # renpy.show_screen('DevEventPopUp')
        globals()['devEventStartRound'] = r
        

    malwareFlag = 0
    pwFlag = 0
    patchFlag = 0
    dataBreachFlag = 0
    def checkDataBreachFactors():
        checkDBPatch()
        if (malwareFlag == 0) and (pwFlag == 0) and (patchFlag == 0):
            globals()['dataBreachFlag'] = 0
        else:
            globals()['dataBreachFlag'] = 1
        if dataBreachFlag == 0:
            globals()['currentDevEvent'] = 2
        else:
            globals()['currentDevEvent'] = 1

    tempDB = []
    installedDB = ''
    def malwareSelection():
        if len(publishedDbs) > 0:
            tempDB.clear()
            for i in range(len(publishedDbs)):
                if publishedDbs[i][0]['malware'] == 'clean':
                    tempDB.append(i)
            if len(tempDB) > 0:
                globals()['installedDB'] = publishedDbs[random.choice(tempDB)][0]['name']
                if round > firstEvent:
                    globals()['malwareFlag'] = 1   ####red dot

    changePWCount = 0
    def pwPolicy():       # shd be ok
        if changePWCount >= (totalMember/2):
            globals()['pwFlag'] = 0
        else:
            globals()['pwFlag'] = 1
        globals()['changePWCount'] = 0

    patchCompletedCount = 0
    def checkDBPatch():   # every 5 round, shd be ok
        globals()['patchFlag'] = 0     # initialize
        globals()['patchCompletedCount'] = 0

        for j in range(len(publishedDbs)):
            if publishedDbs[j][0]['name'] == installedDB:
                for i in range(len(publishedDbs[j][0]['patch'])):
                    if publishedDbs[j][0]['patch'][i][1]['patch-status'] == 'patch completed':
                        globals()['patchCompletedCount'] += 1
                if patchCompletedCount >= len(publishedDbs[j][0]['patch'])/2:
                    globals()['patchFlag'] = 0
                else:
                    globals()['patchFlag'] = 1

    hackedMember = ''
    def hackedMemberSet(name):
        globals()['hackedMember'] = name

    pwMsg = "change passwords regularly"
    malwareMsg = "uninstall detected malware as soon as possible"
    patchMsg = "apply patches to protect data"
    fullMsg = ""
    def dbMsg():
        globals()['fullMsg'] = ""
        if pwFlag == 1:
            if len(fullMsg) == 0:
                globals()['fullMsg'] = pwMsg
            else:
                globals()['fullMsg'] = fullMsg + ", " + pwMsg
        if malwareFlag == 1:
            if len(fullMsg) == 0:
                globals()['fullMsg'] = malwareMsg
            else:
                globals()['fullMsg'] = fullMsg + ", " + malwareMsg
        if patchFlag == 1:
            if len(fullMsg) == 0:
                globals()['fullMsg'] = patchMsg
            else:
                globals()['fullMsg'] = fullMsg + ", " + patchMsg

define summaryMsgList = [
        'You should complete all essential features before the app can be released in the market.',
        'Wow, you have gained a great number of users in the latest release of UberLock. Please try to develop more features to enhance user experience and satisfaction.',
        'Please keep on developing more features to attract more potential users!',
        'App users enjoy the features of the app! Well done!',
        'Users are worried about the data privacy issues. Please try your best to secure the databases!',
        'Exposed data may lead to dramatic drop of number of users and revenue. Please identify the security risks, and solve them immediately!']

transform flash:
    on show:
        alpha 1.0 
        linear 0.5 alpha 0.0 
        linear 0.5 alpha 1.0 
        repeat

screen LoadingScreen():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 450, 300) xalign 0.5 yalign 0.5
    add "loading_dark.png" align (0.5, 0.5) zoom 0.5
    add "loading.png" at flash align (0.5, 0.5) zoom 0.5
    text "{color=#000000} %s {/color}"%loadingAction xalign 0.5 yalign 0.65 size 30


screen TransparentScreen_00():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('Lightbulb_On'), Hide('EventsPopUp'), Hide('DevEventPopUp'), Hide('RatioRecommend')]

screen Lightbulb_On():
    imagebutton idle im.Scale("notification.png", 45, 45) xalign 0.775 yalign 0.005 action [Hide('Lightbulb_On')]

screen Lightbulb_Off():
    imagebutton idle im.Scale("notification_dull.png", 45, 45) xalign 0.775 yalign 0.005 action Function(reOpenLightbulb)


screen EventsPopUp():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 530) xalign 0.5 yalign 0.5    
    text "{color=#000000} {b} %s {/b} {/color}"%currentEvent['name'] xalign 0.51 yalign 0.18 size 20


    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.17 action [Hide('EventsPopUp'), Hide('Lightbulb_On')]
    add im.Scale(currentEvent['image'], 300, 300) xalign 0.5 yalign 0.5
    vbox:
        xmaximum 450
        xanchor 0
        yanchor 0
        xalign 0.33
        yalign 0.73
        text "{color=#000000} %s {/color}"%currentEvent['content'] size 18

screen PatchDetails():
    add im.Scale("panel_white.png", 420, 250) xalign 0.5 yalign 0.5
    add im.Scale("panel_black.png", 390, 2) xalign 0.5 yalign 0.37
    text "{color=#000000} Vulnerability Details - %s {/color}"%patches[patchDetail]['name'] xalign 0.44 yalign 0.349 size 14
    text "{color=#000000} Patch ID: %s {/color}"%patches[patchDetail]['id'] xalign 0.62 yalign 0.352 size 10
    imagebutton idle im.Scale("exit.png", 20, 20) xalign 0.65 yalign 0.35 action Hide('PatchDetails')

    text "{color=#000000} - Affected DBs(s): {b} %s {/b} {/color}"%patches[patchDetail]['impacts'] xalign 0.46 yalign 0.38 size 12

    vbox:
        xalign 0.44 yalign 0.44
        xminimum 200

        text "{color=#000000} Type:  {/color}" size 10
        text "{color=#000000} Criticality:  {/color}" size 10
        text "{color=#000000} Expected Testing Time:  {/color}" size 10
        text "{color=#000000} Deployment Time:  {/color}" size 10
        text "{color=#000000} Patch Installation Time:  {/color}" size 10

    vbox:
        xalign 0.6 yalign 0.44
        xminimum 200

        text "{color=#000000} %s  {/color}"%patches[patchDetail]['details'][0]['Type'] size 10
        text "{color=#000000} %s  {/color}"%patches[patchDetail]['details'][1]['Criticality'] size 10
        text "{color=#000000} %s  {/color}"%patches[patchDetail]['details'][2]['Expected-Testing-Time'] size 10
        text "{color=#000000} %s  {/color}"%patches[patchDetail]['details'][3]['Deployment-Time'] size 10
        text "{color=#000000} %s  {/color}"%patches[patchDetail]['details'][4]['Patch-Installation-Time'] size 10


screen ConfirmPatchTesting():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 283) xalign 0.5 yalign 0.5
    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.35 action Hide('ConfirmPatchTesting')
    
    text "{color=#000000} {b} Test Patch %s {/b} {/color}"%patches[patchDetail]['id'] xalign 0.51 yalign 0.4 size 20
    text "{color=#000000} Do you want to test patch %s? {/color}" %patches[patchDetail]['id'] xalign 0.5 yalign 0.47 size 15
    text "{color=#000000} This process costs %s round(s). {/color}"%patches[patchDetail]['details'][2]['Expected-Testing-Time'] xalign 0.5 yalign 0.5 size 15

    imagebutton idle im.Scale("UI_popup_cancel.png", 200, 55) xalign 0.4 yalign 0.59 action [Hide('ConfirmPatchTesting'), Hide('inProgressPopWindow')]
    text "{color=#000000} Cancel {/color}" xalign 0.41 yalign 0.58 size 15
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.6 yalign 0.59 action [Function(startPatchTest), Hide('PatchDetails')]
    text "{color=#ffffff} {b} Test Patch {/b} {/color}" xalign 0.595 yalign 0.58 size 15


screen ConfirmPatch():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 283) xalign 0.5 yalign 0.5
    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.35 action Hide('ConfirmPatch')

    text "{color=#000000} {b} Finish testing the patch {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    text "{color=#000000} Install the Patch now? {/color}" xalign 0.5 yalign 0.47 size 15
    text "{color=#000000} This process costs %s round(s). {/color}"%patches[patchDetail]['details'][4]['Patch-Installation-Time'] xalign 0.5 yalign 0.5 size 15
    text "{color=[color_critical]} All other feature development{/color}{color=[color_critical]} will be halted {/color}" xalign 0.5 yalign 0.524 size 15 text_align 0

    imagebutton idle im.Scale("UI_popup_cancel.png", 200, 55) xalign 0.4 yalign 0.61 action Hide('ConfirmPatch')
    text "{color=#000000} Later {/color}" xalign 0.41 yalign 0.6 size 15
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.6 yalign 0.61 action Function(startpatch)
    text "{color=#ffffff} {b} Install Patch {/b} {/color}" xalign 0.595 yalign 0.6 size 15



screen DevEventPopUp():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 700) xalign 0.53 yalign 0.99
    text "{color=#000000} {b} %s {/b} {/color}"%events[currentDevEvent]['name'] xalign 0.53 yalign 0.12 size 20
    
    if currentDevEvent != 0 and currentDevEvent != 1 and currentDevEvent != 2:
        imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.14 action [Hide('DevEventPopUp'), Hide('Lightbulb_On')]
    add im.Scale(events[currentDevEvent]['image'], 250, 230) xalign 0.5 yalign 0.26
    
    if dataBreachFlag == 1 and currentDevEvent == 1:
        vbox:
            xmaximum 300
            xanchor 0
            yanchor 0
            xalign 0.40
            yalign 0.51
            
            if len(fullMsg) > 0:
                text "{color=#FF0000} Please %s.{/color}"%fullMsg yalign 0.2 size 14

            for k in range(len(publishedDbs)):
                if publishedDbs[k][0]['name'] == installedDB:
                    text "{color=#000000} Attacked database: %s {/color}"%publishedDbs[k][0]['name'] size 14
                    for i in range(len(tables)):
                        if tables[i]['name'] == publishedDbs[k][0]['name']:
                            $ tempAffected = numOfUsers*int(tables[i]['popularity'])/5
                            text "{color=#000000} Impact: %d user records are exposed{/color}"%tempAffected size 14
                            text "{color=#000000} Leaked data: {/color}" size 14
                            for j in range(len(tables[i]['items'])):
                                text "{color=#000000} %s {/color}"%tables[i]['items'][j]['name'] size 14
                            add im.Scale("UI_tab_line.png", 400, 55) xpos -45 ypos +10
                            hbox:
                                spacing 10
                                add im.Scale("indicator_user.png", 25, 25) ypos -30
                                text "{color=#d20000} -%d {/color}"%tempAffected size 14 ypos -23
                                add im.Scale("indicator_satisfaction.png", 25, 25) ypos -30
                                text "{color=#d20000} -%d {/color}"%(int(tables[i]['popularity'])*10) size 14 ypos -23
                                add im.Scale("indicator_leakage.png", 25, 25) ypos -30
                                text "{color=#d20000} +%d {/color}"%int(tables[i]['sensitivity']) size 14 ypos -23
                                add im.Scale("indicator_revenue.png", 25, 25) ypos -30
                                text "{color=#d20000} -%d {/color}"%(tempAffected*20) size 14 ypos -23
                            imagebutton idle im.Scale("UI_popup_okay.png", 180, 40) xalign 0.25 action [Hide('DevEventPopUp'), Hide('Lightbulb_On'),Function(dataBreachCal,tempAffected,int(tables[i]['popularity']),int(tables[i]['sensitivity']))]
                            text "{color=#ffffff} Okay {/color}" xalign 0.35 ypos -30 size 14

    elif currentDevEvent == 2:
        vbox:
            xmaximum 420
            xanchor 0
            yanchor 0
            xalign 0.35
            yalign 0.6

            text "{color=#000000} %s {/color}"%events[currentDevEvent]['content'] size 18
            text "{color=#000000} You have successfully released %d features! Well done! {/color}"%len(publishedFeatures) size 18
            add im.Scale("UI_tab_line.png", 400, 55) xpos -5 ypos +10
            hbox:
                xalign 0.4
                spacing 10
                add im.Scale("indicator_user.png", 25, 25) ypos -30
                text "{color=#66ff33} +%d {/color}"%(10*len(publishedFeatures)) size 14 ypos -23
                add im.Scale("indicator_satisfaction.png", 25, 25) ypos -30
                text "{color=#66ff33} +15 {/color}" size 14 ypos -23
                add im.Scale("indicator_leakage.png", 25, 25) ypos -30
                text "{color=#66ff33} +0 {/color}" size 14 ypos -23
                add im.Scale("indicator_revenue.png", 25, 25) ypos -30
                text "{color=#66ff33} +%d {/color}"%(15*len(publishedFeatures)) size 14 ypos -23
            imagebutton idle im.Scale("UI_popup_okay.png", 180, 40) xalign 0.45 action [Hide('DevEventPopUp'), Hide('Lightbulb_On'),Function(rewardCal)]
            text "{color=#ffffff} Okay {/color}" xalign 0.45 ypos -30 size 14

    elif currentDevEvent == 0:
        vbox:
            xmaximum 420
            xanchor 0
            yanchor 0
            xalign 0.35
            yalign 0.6
            spacing 15

            text "{color=#3366FF}%s {/color}{color=#000000}leaves the team today.{/color}"%resignMember size 18
            text "{color=#000000}Please choose another team member to take over the former duties. {/color}" size 18

            imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 action [Hide('DevEventPopUp'), Hide('Lightbulb_On'), Show('revokeReminder')]
            text "{color=#ffffff} {b}Proceed{/b} {/color}" xalign 0.5 ypos -55 size 14
    else:
        vbox:
            xmaximum 420
            xanchor 0
            yanchor 0
            xalign 0.35
            yalign 0.6

            text "{color=#000000} %s {/color}"%events[currentDevEvent]['content'] size 18

screen revokeReminder():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 600, 250) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Attention: Revoke Team Member {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    
    text "{color=#d20000} Please remove the access permission given to [resignMember]{/color}" xalign 0.5 yalign 0.45 size 17 text_align 0.5 line_spacing 2
    text "{color=#000000} and{/color}{color=#3366FF} select a new member{/color}{color=#000000} to perform [resignMember]'s duties \n within 5 rounds.{/color}" xalign 0.5 yalign 0.51 size 17 text_align 0.5 line_spacing 2
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action Function(revokeReminderOkay)
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15

screen revokeWarning():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 600, 400) xalign 0.5 yalign 0.55
    text "{color=#000000} {b} Penalty: Fail to Revoke Former Team Member {/b} {/color}" xalign 0.51 yalign 0.37 size 20
    
    text "{color=#000000} As failed to remove access of [resignMember], \nsome data ae loss due to outsider attack. {/color}" xalign 0.5 yalign 0.48 size 17 text_align 0.5 line_spacing 2
    text "{color=#000000} Right now removing the access for [resignMember] is done for you. {/color}" xalign 0.5 yalign 0.55 size 17 text_align 0.5 line_spacing 2

    add im.Scale("ui_top.png", 400, 40) xalign 0.5 yalign 0.63
    
    hbox:
        xalign 0.5 yalign 0.67
        spacing 10
        add im.Scale("indicator_user.png", 25, 25) ypos -30
        text "{color=#d20000} - %d {/color}"%(10*len(publishedFeatures)) size 14 ypos -23
        add im.Scale("indicator_satisfaction.png", 25, 25) ypos -30
        text "{color=#d20000} - 20 {/color}" size 14 ypos -23
        add im.Scale("indicator_leakage.png", 25, 25) ypos -30
        text "{color=#d20000} - 200 {/color}" size 14 ypos -23
        add im.Scale("indicator_revenue.png", 25, 25) ypos -30
        text "{color=#d20000} - %d {/color}"%(15*len(publishedFeatures)) size 14 ypos -23

    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.75 action [Hide('revokeWarning'), Function(failRevokePenalty), Function(revokeConfirm, resignMember)]
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.74 size 15



screen logReportPopup():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 900, 550) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Log Report {/b} {/color}" xalign 0.5 yalign 0.15 size 20

    vbox:
        xalign 0.5
        yalign 0.5
        xmaximum 800
        spacing 10

        grid 4 12:
            xspacing 10
            yspacing 10
            xmaximum 225

            text "{color=#000000}{b} Date/ Time {/b}{/color}" size 14
            text "{color=#000000}{b} Who {/b}{/color}" size 14
            text "{color=#000000}{b} Event ID {/b}{/color}" size 14
            text "{color=#000000}{b} Event Message {/b}{/color}" size 14

            $now = datetime.now()-timedelta(minutes=5)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            #$hackedMember1 = random.choice(members)['name']
            $temphackRisk = [10,10,10,10,10,10,10,10]
            for x in range(len(members)):
                if teamList[x] == 'Development' or teamList[x] == 'Production':
                    $temphackRisk[x] = members[x]['pwStrength']
            $riskIndex = 10
            $riskValue = 10
            for y in range(len(temphackRisk)):
                if temphackRisk[y]<riskValue:
                    $riskValue = temphackRisk[y]
                    $riskIndex = y
            $hackedMember1 = members[riskIndex]['name']
            # else:
            #     $hackedMember1 = random.choice(members)['name']

            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E3 {/color}" size 14
            text "{color=#000000} Unsuccessful login {/color}" size 14

            $now = datetime.now()-timedelta(minutes=4,seconds=30)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E3 {/color}" size 14
            text "{color=#000000} Unsuccessful login {/color}" size 14

            $now = datetime.now()-timedelta(minutes=4)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E3 {/color}" size 14
            text "{color=#000000} Unsuccessful login {/color}" size 14

            $now = datetime.now()-timedelta(minutes=3,seconds=30)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E3 {/color}" size 14
            text "{color=#000000} Unsuccessful login {/color}" size 14

            $now = datetime.now()-timedelta(minutes=3)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E1 {/color}" size 14
            text "{color=#000000} Login {/color}" size 14

            $now = datetime.now()-timedelta(minutes=2,seconds=15)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            $tempMember = []
            text "{color=#000000} [dt_string] {/color}" size 14
            for j in range(len(members)):
                if members[j]['name']!=hackedMember1 and teamList[j] != 'Team' and teamList[j] != '-':
                    $tempMember.append(members[j]['name'])
            if len(tempMember)>0:
                $hackedMember2 = random.choice(tempMember)
            else:
                $hackedMember2 = random.choice(members)['name']
            text "{color=#000000} Uber\%s {/color}"%hackedMember2 size 14
            text "{color=#000000} E1 {/color}" size 14
            text "{color=#000000} Login {/color}" size 14

            $now = datetime.now()-timedelta(minutes=2)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E6 {/color}" size 14
            text "{color=#000000} Modify user account {/color}" size 14

            $now = datetime.now()-timedelta(minutes=1,seconds=30)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E7 {/color}" size 14
            text "{color=#000000} Create role {/color}" size 14

            $now = datetime.now()-timedelta(minutes=1,seconds=15)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\%s {/color}"%hackedMember2 size 14
            text "{color=#000000} E13 {/color}" size 14
            text "{color=#000000} Modify object {/color}" size 14

            $now = datetime.now()-timedelta(minutes=1)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E7 {/color}" size 14
            text "{color=#000000} Create role {/color}" size 14

            $now = datetime.now()-timedelta(seconds=30)
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} [dt_string] {/color}" size 14
            text "{color=#000000} Uber\{color=#FF0000}%s{/color} {/color}"%hackedMember1 size 14
            text "{color=#000000} E7 {/color}" size 14
            text "{color=#000000} Create role {/color}" size 14

        text "{color=#3366FF} Suspicious login attemps of [hackedMember1] are found. {/color}" xalign 0.5 size 14
        text "{color=#3366FF} Please change members' passwords regularly. {/color}" xalign 0.5 size 14


        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 action [Hide('logReportPopup'),Function(hackedMemberSet,hackedMember1)]
        text "{color=#ffffff} Okay {/color}" xalign 0.5 ypos -45 size 14

screen auditReportPopup():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 800, 530) xalign 0.5 yalign 0.5
    text "{color=#000000} Audit Report {/color}" xalign 0.5 yalign 0.17 size 20
    imagebutton idle im.Scale("exit.png", 40, 40) xalign 0.67 yalign 0.17 action Hide('auditReportPopup')

    vbox:
        xalign 0.5
        yalign 0.5
        xmaximum 700
        spacing 10

        text "{color=#000000} Possible malware activity {/color}" size 20
        text "{color=#000000} The alert was triggered by 150 activity records being captured within 60 seconds. The most recent of those activity records is shown below. {/color}" size 16

        grid 2 9:
            xspacing 10
            xmaximum 350

            text "{color=#000000} Who: {/color}" size 14
            $tempMemberList = []
            for i in range(len(teamList)):
                if teamList[i] == 'Production':
                    $tempMemberList.append(i)
            if len(tempMemberList)>0:
                $tempRandomMember = members[random.choice(tempMemberList)]['name']
            else:
                $tempRandomMember = members[random.randint(0,len(members)-1)]['name']
            text "{color=#000000} Uber\%s{/color}"%tempRandomMember size 14

            text "{color=#000000} Action: {/color}" size 14
            text "{color=#000000} Modified {/color}" size 14

            text "{color=#000000} Object type: {/color}" size 14
            text "{color=#000000} File {/color}" size 14

            for j in range(len(publishedDbs)):
                if publishedDbs[j][0]['name'] == installedDB:
                    $tempDBname = publishedDbs[j][0]['name']
            for i in range(len(tables)):
                if tables[i]['name'] == tempDBname:
                    $tempAttributes = random.choice(tables[i]['items'])['name']
            text "{color=#000000} What: {/color}" size 14
            text "{color=#000000} C:\Windows\%s\{color=#FF0000}%s{/color}\%s.exe {/color}"%(tempRandomMember,tempDBname,tempAttributes) size 14

            $now = datetime.now()
            $dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            text "{color=#000000} When: {/color}" size 14
            text "{color=#000000} [dt_string] {/color}" size 14

            text "{color=#000000} Where: {/color}" size 14
            text "{color=#FF0000}%s{/color} {color=#000000} database {/color}"%tempDBname size 14

            text "{color=#000000} Workstation: {/color}" size 14
            text "{color=#000000} ws%d.uber.com {/color}"%random.randint(1, 30) size 14

            text "{color=#000000} Data source: {/color}" size 14
            text "{color=#000000} File Servers {/color}" size 14

            text "{color=#000000} Details: {/color}" size 14
            text "{color=#000000} Size changed from %d bytes to %d bytes {/color}"%(random.randint(800000, 900000),random.randint(800000, 900000)) size 14
    

screen summaryPopUp():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 550, 400) xalign 0.5 yalign 0.5

    text "{color=#000000} {b} Round [round]: Progress Summary {/b} {/color}" xalign 0.5 yalign 0.28 size 20
    add im.Scale("summary_bar.png", 480, 50) xalign 0.5 yalign 0.36
    hbox:
        xalign 0.53
        yalign 0.37
        spacing 80
        text "{color=#ffffff} [numOfUsers] {/color}" size 16 text_align 0
        text "{color=#ffffff} [userSat] {/color}" size 16 text_align 0
        text "{color=#ffffff} [dataLeak] {/color}" size 16 text_align 0
        text "{color=#ffffff} [revenue] {/color}" size 16 text_align 0
    
    add im.Scale("UI_tab_dark.png", 470, 50) xalign 0.5 yalign 0.45
    text "{color=#ffffff} No. of released features: {/color}" xalign 0.41 yalign 0.45 size 16 text_align 0
    text "{color=#ffffff} %d/ 9 features {/color}"%len(publishedFeatures) xalign 0.6 yalign 0.45 size 16 text_align 1

    add im.Scale("UI_tab_dark.png", 470, 50) xalign 0.5 yalign 0.52
    text "{color=#ffffff} Revenue generated per round: {/color}" xalign 0.43 yalign 0.52 size 16 text_align 0
    $ feat_pop = 0
    for i in range(len(publishedFeatures)):
        for j in range(len(tables)):
            if publishedFeatures[i]==tables[j]['name']:
                $ feat_pop += int(tables[j]['popularity'])
    text "{color=#ffffff} $%d {/color}"%((numOfUsers+feat_pop)*15) xalign 0.62 yalign 0.52 size 16 text_align 1
    
    hbox:
        xalign 0.5
        yalign 0.62
        xmaximum 470
        text "{color=#000000} %s {/color}"%summaryMsg size 14 text_align 0.5

    imagebutton idle im.Scale("UI_popup_okay.png", 150, 40) xalign 0.5 yalign 0.7 action [Hide('summaryPopUp'), Hide('Lightbulb_On')]
    text "{color=#ffffff} Okay {/color}" xalign 0.5 yalign 0.69 size 14