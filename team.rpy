init python:
    # total number of members
    totalMember = 8

    # ratio of manpower required
    dev = 6
    pdt = 2

    # member's team
    teamList = ['Team','Team','Team','Team','Team','Team','Team','Team']
    toBeTeamList = ['Team','Team','Team','Team','Team','Team','Team','Team']

    # role's team
    roleList = ['Role','Role','Role','Role','Role','Role','Role','Role']
    toBeRoleList = ['Role','Role','Role','Role','Role','Role','Role','Role']

    def team_dropdown(x):
        renpy.show_screen('team_dropdown'+str(x))

    def role_dropdown(x):
        if x==0:
            renpy.show_screen('role_dropdown0')
        if x==1:
            renpy.show_screen('role_dropdown1')
        if x==2:
            renpy.show_screen('role_dropdown2')
        if x==3:
            renpy.show_screen('role_dropdown3')
        if x==4:
            renpy.show_screen('role_dropdown4')
        if x==5:
            renpy.show_screen('role_dropdown5')
        if x==6:
            renpy.show_screen('role_dropdown6')
        if x==7:
            renpy.show_screen('role_dropdown7')

    def changeTeam(member,team):
        if team == 0 and toBeTeamList[member] == 'Development':
            toBeTeamList[member] = 'Team'
            return
        elif team == 1 and toBeTeamList[member] == 'Production':
            toBeTeamList[member] = 'Team'
            return
        if team==0:
            toBeTeamList[member]='Development'
        else:
            toBeTeamList[member]='Production'

    def changeRole(member,role):
        if role == 0 and toBeRoleList[member] == 'DBA':
            toBeRoleList[member] = 'Role'
            return
        elif role == 1 and toBeRoleList[member] == 'Developer':
            toBeRoleList[member] = 'Role'
            return
        if role==0:
            toBeRoleList[member] = 'DBA'
        else:
            toBeRoleList[member] = 'Developer'

    def confirmTeamRole():
        if checkRoles():
            for i in range(len(teamList)):
                teamList[i] = toBeTeamList[i]
                roleList[i] = toBeRoleList[i]
            if not checkMemberRatio():
                renpy.show_screen('RatioRecommend')
            renpy.show_screen('confirmTeamRoles')
            addRounds()
            return
        else: return


    def manpowerCal():
        if totalMember == 8:
            if (100*(len(deployedFeatures)+len(publishedFeatures))/9) > (100*2/totalMember) and (100*(len(deployedFeatures)+len(publishedFeatures))/9) < (100*6/totalMember):
                globals()['pdt'] = totalMember * (100*(len(deployedFeatures)+len(publishedFeatures))/9)
                globals()['dev'] = totalMember - pdt/100
                globals()['pdt'] = totalMember - dev
        elif totalMember == 7:
            globals()['pdt'] = totalMember * (100*(len(deployedFeatures)+len(publishedFeatures))/9)
            globals()['dev'] = totalMember - pdt/100
            globals()['pdt'] = totalMember - dev
            if pdt<2:
                globals()['pdt'] = 2
                globals()['dev'] = totalMember - pdt
            elif dev<2:
                globals()['dev'] = 2
                globals()['pdt'] = totalMember - dev
        reduceFraction(dev, pdt)

    hcf = 0
    smaller = 0
    reducedDev = 3
    reducedPdt = 1
    def reduceFraction(x, y) :
        if x > y:
            smaller = y
        else:
            smaller = x
        for i in range(1, smaller+1):
            if ((x % i == 0) and (y % i == 0)):
                hcf = i
        x = x // hcf
        y = y // hcf
        globals()['reducedDev'] = x
        globals()['reducedPdt'] = y

    revokeMember = ""
    revokeMemberIndex = 0
    revokeXpos = 0
    revokeCompete = False
    tempRevokeMember = ""
    revokeFlag = 0
    revokeDeadline = 0

    def checkRevokeMember(member):
        globals()['tempRevokeMember'] = members[member]['name']
        renpy.show_screen('revokeConfirmPopup')

    def revokeConfirm(member):
        globals()['revokeMember'] = member
        for x in range(len(members)):
            if members[x]['name'] == member:
                globals()['revokeMemberIndex'] = x
        globals()['totalMember'] -= 1
        manpowerCal()
        teamList[revokeMemberIndex] = '-'
        roleList[revokeMemberIndex] = '-'
        toBeTeamList[revokeMemberIndex] = '-'
        toBeRoleList[revokeMemberIndex] = '-'
        revokeMemberPos(revokeMemberIndex)
        revokeCal(round)
        globals()['revokeFlag'] = 1

        if not checkMinRoles():
            ui.timer(5.0, Show("missingRoles"))
        if checkTeamRoleMatching() > 0:
            ui.timer(2.0, Show("teamRoleNotMatching"))

    def revokeMemberPos(member):
        #if not checkMinRoles(): return

        if member == 0:
            globals()['revokeXpos'] = 20
        elif member == 1:
            globals()['revokeXpos'] = 175
        elif member == 2:
            globals()['revokeXpos'] = 330
        elif member == 3:
            globals()['revokeXpos'] = 485
        elif member == 4:
            globals()['revokeXpos'] = 640
        elif member == 5:
            globals()['revokeXpos'] = 795
        elif member == 6:
            globals()['revokeXpos'] = 950
        elif member == 7:
            globals()['revokeXpos'] = 1105

        globals()['revokeCompete'] = True
        if not checkMemberRatio():
            renpy.show_screen('RatioRecommend')
        addRounds()

    def revokeReminderOkay():
        renpy.hide_screen('revokeReminder')
        renpy.hide_screen('EventsPopUp')
        globals()['revokeDeadline'] = round + 5

    def failRevokePenalty():
        globals()['numOfUsers'] -= 10 * len(publishedFeatures)
        globals()['userSat'] -= 20
        globals()['dataLeak'] += 30
        globals()['revenue'] -= 15 * len(publishedFeatures)


    resignMember = ""
    resignIndex = 0
    resignChoice = []
    def chooseResign():
        for x in range(len(teamList)):
            if teamList[x] != 'Team' and teamList[x] != '-':
                resignChoice.append(x)
        globals()['resignIndex'] = random.choice(resignChoice)
        globals()['resignMember'] = members[resignIndex]['name']

    def checkRoles():
        if not checkMinRoles():
            renpy.show_screen('missingRoles')
            return False
        if checkTeamRoleMatching() > 0:
            renpy.show_screen('teamRoleNotMatching')
            return False
        return True

    def checkMinRoles():
        DevTeam = []
        ProTeam = []

        for member in range(len(toBeTeamList)):
            if toBeTeamList[member] == 'Development':
                DevTeam.append(toBeRoleList[member])
            elif toBeTeamList[member] == 'Production':
                ProTeam.append(toBeRoleList[member])
        if 'DBA' in DevTeam and 'Developer' in DevTeam and 'DBA' in ProTeam and 'Developer' in ProTeam:
            return True
        else:
            return False

    def checkAssign():
        for x in range(len(members)):
            if members[x]['name'] == tempRevokeMember:
                if teamList[x] == 'Team' or teamList[x] == '-':
                    return False
                if roleList[x] == 'Role' or roleList[x] == '-':
                    return False
        return True

    def checkRevokeCorrect():
        if tempRevokeMember == resignMember:
            return True
        return False
    
    def checkTeamRoleMatching():
        for i in range(len(toBeTeamList)):
            if toBeTeamList[i] != 'Team' and toBeRoleList[i] == 'Role':
                return i
            elif toBeTeamList[i] == 'Team' and toBeRoleList[i] != 'Role':
                return i
        return -1

    def isTeamChanged():
        for i in range(len(teamList)):
            if toBeTeamList[i] != teamList[i] or toBeRoleList[i] != roleList[i]:
                return True
        return False

    def resetTeamRole():
        for i in range(len(teamList)):
            toBeTeamList[i] = teamList[i]
            toBeRoleList[i] = roleList[i]

    def checkMemberRatio():
        currentDev = toBeTeamList.count('Development')
        currentPdt = toBeTeamList.count('Production')
        if currentDev == 0 or currentPdt == 0:
            return False
        elif dev/pdt == currentDev/currentPdt:
            return True
        else:
            return False




screen team():
    add im.Scale("UI_screen_team.png", 1285, 620) xalign 0 yalign 0.99

    # ratio
    text "{color=#000000}Target Manpower Ratio{/color}" xalign 0.5 yalign 0.165
    text "{color=#000000}Development{/color}" xpos 160 yalign 0.205 size 14
    text "{color=#000000}Production{/color}" xpos 1060 yalign 0.205 size 14
    add im.Scale("panel_orange.png",1000,30) xpos 150 yalign 0.235
    add im.Scale("panel_greenblue.png",1000/totalMember*dev,30) xpos 150 yalign 0.235
    text "{color=#000000}[reducedDev]{/color}" xpos 160 yalign 0.285
    text "{color=#000000}[reducedPdt]{/color}" xpos 1120 yalign 0.285

    # member 1
    image im.Scale("staff_tommy.png", 100, 100) xalign 0.05 yalign 0.4
    image im.Scale("color_tommy.png", 100, 30) xalign 0.05 yalign 0.5
    text "{color=#ffffff}Tommy{/color}" xalign 0.07 yalign 0.505  size 12
    image im.Scale("member_1.png", 130, 120) xalign 0.035 yalign 0.7

    # member 2
    image im.Scale("staff_clara.png", 100, 100) xalign 0.18 yalign 0.4
    image im.Scale("color_clara.png", 100, 30) xalign 0.18 yalign 0.5
    text "{color=#ffffff}Clara{/color}" xalign 0.195 yalign 0.505  size 12
    image im.Scale("member_2.png", 130, 120) xalign 0.17 yalign 0.7

    # member 3
    image im.Scale("staff_ben.png", 100, 100) xalign 0.31 yalign 0.4
    image im.Scale("color_ben.png", 100, 30) xalign 0.31 yalign 0.5
    text "{color=#ffffff}Ben{/color}" xalign 0.32 yalign 0.505  size 12
    image im.Scale("member_3.png", 130, 120) xalign 0.3 yalign 0.7

    # member 4
    image im.Scale("staff_rebecca.png", 100, 100) xalign 0.44 yalign 0.4
    image im.Scale("color_rebecca.png", 100, 30) xalign 0.44 yalign 0.5
    text "{color=#ffffff}Rebecca{/color}" xalign 0.445 yalign 0.505  size 12
    image im.Scale("member_4.png", 130, 120) xalign 0.435 yalign 0.7

    # member 5
    image im.Scale("staff_joseph.png", 100, 100) xalign 0.57 yalign 0.4
    image im.Scale("color_joseph.png", 100, 30) xalign 0.57 yalign 0.5
    text "{color=#ffffff}Joseph{/color}" xalign 0.565 yalign 0.505  size 12
    image im.Scale("member_5.png", 130, 120) xalign 0.57 yalign 0.7

    # member 6
    image im.Scale("staff_peter.png", 100, 100) xalign 0.70 yalign 0.4
    image im.Scale("color_peter.png", 100, 30) xalign 0.70 yalign 0.5
    text "{color=#ffffff}Peter{/color}" xalign 0.69 yalign 0.505  size 12
    image im.Scale("member_6.png", 130, 120) xalign 0.7 yalign 0.7

    # member 7
    image im.Scale("staff_mandy.png", 100, 100) xalign 0.83 yalign 0.4
    image im.Scale("color_mandy.png", 100, 30) xalign 0.83 yalign 0.5
    text "{color=#ffffff}Mandy{/color}" xalign 0.815 yalign 0.505  size 12
    image im.Scale("member_7.png", 130, 120) xalign 0.835 yalign 0.7

    # member 8
    image im.Scale("staff_lex.png", 100, 100) xalign 0.96 yalign 0.4
    image im.Scale("color_lex.png", 100, 30) xalign 0.96 yalign 0.5
    text "{color=#ffffff}Lex{/color}" xalign 0.93 yalign 0.505  size 12
    image im.Scale("member_8.png", 130, 120) xalign 0.97 yalign 0.7

    # salary
    hbox:
        xpos 60
        ypos 380
        spacing 49
        for i in range(8):
            text "{color=#000000}Salary: $ 500/ 10 rounds{/color}"  size 8


    # revoke button
    if round >= firstEvent and revokeFlag != 1:
        hbox:
            xpos 150 ypos 225
            xanchor 0 yanchor 0
            spacing 128
            for member in range (len(teamList)):
                if teamList[member] != "Team" and roleList[member] != "-":
                    imagebutton idle im.Scale("exit.png", 25, 25) action Function(checkRevokeMember, member)
                else:
                    imagebutton idle im.Scale("transparent_00.png", 25, 25) action NullAction()




    # capability label
    hbox:
        spacing 69
        for i in range (8):
            text "{color=#000000}Programming Skills{/color}" xpos 70 ypos 410 size 8
    hbox:
        spacing 112
        for i in range (8):
            text "{color=#000000}Customer \n  Comm{/color}" xpos 20 ypos 435 size 8
    hbox:
        spacing 130
        for i in range (8):
            text "{color=#000000}DB & \nInfra{/color}" xpos 150 ypos 435 size 8
    hbox:
        spacing 110
        for i in range (8):
            text "{color=#000000}Testing &\n Quality\nAssurance{/color}" xpos 50 ypos 545 size 8
    hbox:
        spacing 110
        for i in range (8):
            text "{color=#000000}Security & \nCompliance{/color}" xpos 110 ypos 545 size 8

    # team drop down menu TITLE
    hbox:
        spacing 55
        for i in range (8):
            if toBeTeamList[i] == 'Team' or toBeTeamList[i] == '-':
                imagebutton idle im.Scale("panel_darkgrey.png", 100, 30) xpos 60 ypos 590 action Function(team_dropdown,i)
            elif toBeTeamList[i] == 'Development':
                imagebutton idle im.Scale("panel_greenblue.png", 100, 30) xpos 60 ypos 590 action Function(team_dropdown,i)
            else:
                imagebutton idle im.Scale("panel_orange.png", 100, 30) xpos 60 ypos 590 action Function(team_dropdown,i)
    hbox:
        #spacing 117
        xpos 62
        ypos 595
        for i in range (8):
            hbox:
                xminimum 155
                xmaximum 135
                xanchor 0
                yanchor 0
                text "{color=#000000}%s{/color}"%toBeTeamList[i] size 14

    # role drop down menu TITLE
    hbox:
        spacing 55
        for i in range (8):
            if toBeRoleList[i] == 'Role' or toBeRoleList[i] == '-':
                imagebutton idle im.Scale("panel_darkgrey.png", 100, 30) xpos 60 ypos 630 action Function(role_dropdown,i)
            elif toBeRoleList[i] == 'Developer':
                imagebutton idle im.Scale("color_rebecca.png", 100, 30) xpos 60 ypos 630 action Function(role_dropdown,i)
            else:
                imagebutton idle im.Scale("color_tommy.png", 100, 30) xpos 60 ypos 630 action Function(role_dropdown,i)


    hbox:
        #spacing 117
        xpos 62
        ypos 635
        for i in range (8):
            hbox:
                xminimum 155
                xmaximum 135
                xanchor 0
                yanchor 0
                text "{color=#000000}%s{/color}"%toBeRoleList[i] size 14

    if revokeCompete:
        imagebutton idle im.Scale("panel_grey_trans.png", 155, 450) xpos revokeXpos ypos 220 action NullAction()

    # confirm team member
    imagebutton idle im.Scale("UI_popup_confirm.png", 100, 30) xalign 0.97 yalign 0.97 action [Function(confirmTeamRole)]
    if isTeamChanged():
        imagebutton idle im.Scale("UI_popup_confirm.png", 100, 30) xalign 0.97 yalign 0.97 action [Function(confirmTeamRole)]
    else:
        imagebutton idle im.Scale("UI_popup_disable.png", 100, 30) xalign 0.97 yalign 0.97 action NullAction()
    text "{color=#000000} Confirm ✔️ {/color}" xalign 0.965 yalign 0.96 size 14

screen selectTeamPriority():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action Hide('selectTeamPriority')
    add im.Scale("UI_popup_bkg.png", 600, 250) xalign 0.5 yalign 0.5
    
    text "{color=#000000} Please select your team members first before development {/color}" xalign 0.5 yalign 0.45 size 18    
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action [Hide('selectTeamPriority')]
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15



screen missingRoles():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action Hide('missingRoles')
    add im.Scale("UI_popup_bkg.png", 600, 250) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Missing Roles {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    
    text "{color=#000000} There should be at least 1 DBA and 1 Developer {/color}" xalign 0.5 yalign 0.465 size 17
    text "{color=#000000} in the Development and Production team. {/color}" xalign 0.5 yalign 0.5 size 17

    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action [Hide('missingRoles')]
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15

screen teamRoleNotMatching():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action Hide('teamRoleNotMatching')
    add im.Scale("UI_popup_bkg.png", 600, 250) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Missing Team/ Roles {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    
    text "{color=#000000} Please assign Team/ Role for {color=#EE5C65}%s{/color} {/color}"%members[checkTeamRoleMatching()]['name'] xalign 0.5 yalign 0.48 size 15
    text "{color=#000000} Please update and press '{color=#3366FF}Confirm{/color}' again {/color}" xalign 0.5 yalign 0.52 size 15
    
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.61 action [Hide('teamRoleNotMatching')]
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.6 size 15

screen RatioRecommend():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 600, 250) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Tips: Team Member Allocation {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    
    text "{color=#000000} You may refer to the team member ratio \nto allocate your team members {/color}" xalign 0.5 yalign 0.48 size 17 text_align 0.5 line_spacing 2
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action [Hide('RatioRecommend')]
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15

screen confirmTeamRoles():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 600, 250) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Team/ Roles Confirmed {/b} {/color}" xalign 0.51 yalign 0.4 size 20
    
    text "{color=#000000} You've selected %d members. {/color}"%(int(staffSalary()) / 500) xalign 0.5 yalign 0.465 size 17
    text "{color=#000000} Their total salary is $%s and will be deducted every 10 rounds. {/color}"%(staffSalary()) xalign 0.5 yalign 0.5 size 17
    
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action [Hide('confirmTeamRoles')]
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15

screen deductSalary():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 500, 283) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Pay day {/b} {/color}" xalign 0.51 yalign 0.4 size 20

    text "{color=#000000} Staff salary, $%d is deducted. {/color}"%staffSalary() xalign 0.5 yalign 0.49 size 15
    imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action Hide('deductSalary')
    text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15


screen revokeConfirmPopup():
    imagebutton idle im.Scale("transparent_02.png", 1300, 800) xalign 0.78 yalign 0.005 action NullAction()
    add im.Scale("UI_popup_bkg.png", 600, 283) xalign 0.5 yalign 0.5
    text "{color=#000000} {b} Confirmation {/b} {/color}" xalign 0.51 yalign 0.4 size 20

    if checkMinRoles() and checkAssign() and checkRevokeCorrect():
        text "{color=#000000} Revoke %s's role {/color}"%tempRevokeMember xalign 0.5 yalign 0.49 size 15
        imagebutton idle im.Scale("UI_popup_cancel.png", 200, 55) xalign 0.4 yalign 0.59 action [Hide('revokeConfirmPopup')]
        text "{color=#000000} Cancel {/color}" xalign 0.41 yalign 0.58 size 15

        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.6 yalign 0.59 action [Hide('revokeConfirmPopup'),Function(revokeConfirm,tempRevokeMember)]
        text "{color=#ffffff} {b} Revoke [revokeMember] {/b} {/color}" xalign 0.595 yalign 0.58 size 15
        
    elif checkMinRoles() == False:
        text "{color=#000000} There should be at least 1 DBA and 1 Developer\n in the Development and Production team. {/color}" xalign 0.5 yalign 0.49 size 15
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action Hide('revokeConfirmPopup')
        text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15

    elif checkRevokeCorrect() == False:
        text "{color=#000000} Revoked a wrong member! %s should be revoked! {/color}"%resignMember xalign 0.5 yalign 0.49 size 15
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action Hide('revokeConfirmPopup')
        text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15
    
    else:
        text "{color=#000000} The revoked member should be assigned with team and role! {/color}" xalign 0.5 yalign 0.49 size 15
        imagebutton idle im.Scale("UI_popup_okay.png", 200, 55) xalign 0.5 yalign 0.59 action Hide('revokeConfirmPopup')
        text "{color=#ffffff} {b}Okay{/b} {/color}" xalign 0.5 yalign 0.58 size 15



screen team_dropdown0():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown0')]

    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.051 yalign 0.89 action [Hide('team_dropdown0'),Function(changeTeam,0,0)]
    text "{color=#000000}Development{/color}" xalign 0.055 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.051 yalign 0.93 action [Hide('team_dropdown0'),Function(changeTeam,0,1)]
    text "{color=#000000}Production{/color}" xalign 0.06 yalign 0.925  size 13

screen team_dropdown1():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown1')]
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.183 yalign 0.89 action [Hide('team_dropdown1'),Function(changeTeam,1,0)]
    text "{color=#000000}Development{/color}" xalign 0.1855 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.183 yalign 0.93 action [Hide('team_dropdown1'),Function(changeTeam,1,1)]
    text "{color=#000000}Production{/color}" xalign 0.192 yalign 0.925  size 13

screen team_dropdown2():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown2')]
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.314 yalign 0.89 action [Hide('team_dropdown2'),Function(changeTeam,2,0)]
    text "{color=#000000}Development{/color}" xalign 0.315 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.314 yalign 0.93 action [Hide('team_dropdown2'),Function(changeTeam,2,1)]
    text "{color=#000000}Production{/color}" xalign 0.32 yalign 0.925  size 13

screen team_dropdown3():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown3')]
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.445 yalign 0.89 action [Hide('team_dropdown3'),Function(changeTeam,3,0)]
    text "{color=#000000}Development{/color}" xalign 0.445 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.445 yalign 0.93 action [Hide('team_dropdown3'),Function(changeTeam,3,1)]
    text "{color=#000000}Production{/color}" xalign 0.447 yalign 0.925  size 13

screen team_dropdown4():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown4')]
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.577 yalign 0.89 action [Hide('team_dropdown4'),Function(changeTeam,4,0)]
    text "{color=#000000}Development{/color}" xalign 0.575 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.577 yalign 0.93 action [Hide('team_dropdown4'),Function(changeTeam,4,1)]
    text "{color=#000000}Production{/color}" xalign 0.576 yalign 0.925  size 13

screen team_dropdown5():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown5')]
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.709 yalign 0.89 action [Hide('team_dropdown5'),Function(changeTeam,5,0)]
    text "{color=#000000}Development{/color}" xalign 0.705 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.709 yalign 0.93 action [Hide('team_dropdown5'),Function(changeTeam,5,1)]
    text "{color=#000000}Production{/color}" xalign 0.705 yalign 0.925  size 13

screen team_dropdown6():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown6')]
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.839 yalign 0.89 action [Hide('team_dropdown6'),Function(changeTeam,6,0)]
    text "{color=#000000}Development{/color}" xalign 0.835 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.839 yalign 0.93 action [Hide('team_dropdown6'),Function(changeTeam,6,1)]
    text "{color=#000000}Production{/color}" xalign 0.833 yalign 0.925  size 13

screen team_dropdown7():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('team_dropdown7')]
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.97 yalign 0.89 action [Hide('team_dropdown7'),Function(changeTeam,7,0)]
    text "{color=#000000}Development{/color}" xalign 0.965 yalign 0.885  size 13
    imagebutton idle im.Scale("panel_lightblue.png", 100, 30) xalign 0.97 yalign 0.93 action [Hide('team_dropdown7'),Function(changeTeam,7,1)]
    text "{color=#000000}Production{/color}" xalign 0.957 yalign 0.925  size 13

# role drop down
screen role_dropdown0():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown0')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.051 yalign 0.95 action [Hide('role_dropdown0'),Function(changeRole,0,0)]
    text "{color=#000000}DBA{/color}" xalign 0.06 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.051 yalign 0.99 action [Hide('role_dropdown0'),Function(changeRole,0,1)]
    text "{color=#000000}Developer{/color}" xalign 0.06 yalign 0.98  size 13

screen role_dropdown1():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown1')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.183 yalign 0.95 action [Hide('role_dropdown1'),Function(changeRole,1,0)]
    text "{color=#000000}DBA{/color}" xalign 0.188 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.183 yalign 0.99 action [Hide('role_dropdown1'),Function(changeRole,1,1)]
    text "{color=#000000}Developer{/color}" xalign 0.192 yalign 0.98  size 13

screen role_dropdown2():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown2')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.314 yalign 0.95 action [Hide('role_dropdown2'),Function(changeRole,2,0)]
    text "{color=#000000}DBA{/color}" xalign 0.314 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.314 yalign 0.99 action [Hide('role_dropdown2'),Function(changeRole,2,1)]
    text "{color=#000000}Developer{/color}" xalign 0.32 yalign 0.98  size 13

screen role_dropdown3():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown3')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.445 yalign 0.95 action [Hide('role_dropdown3'),Function(changeRole,3,0)]
    text "{color=#000000}DBA{/color}" xalign 0.437 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.445 yalign 0.99 action [Hide('role_dropdown3'),Function(changeRole,3,1)]
    text "{color=#000000}Developer{/color}" xalign 0.447 yalign 0.98  size 13

screen role_dropdown4():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown4')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.577 yalign 0.95 action [Hide('role_dropdown4'),Function(changeRole,4,0)]
    text "{color=#000000}DBA{/color}" xalign 0.561 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.577 yalign 0.99 action [Hide('role_dropdown4'),Function(changeRole,4,1)]
    text "{color=#000000}Developer{/color}" xalign 0.576 yalign 0.98  size 13

screen role_dropdown5():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown5')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.709 yalign 0.95 action [Hide('role_dropdown5'),Function(changeRole,5,0)]
    text "{color=#000000}DBA{/color}" xalign 0.687 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.709 yalign 0.99 action [Hide('role_dropdown5'),Function(changeRole,5,1)]
    text "{color=#000000}Developer{/color}" xalign 0.705 yalign 0.98  size 13

screen role_dropdown6():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown6')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.839 yalign 0.95 action [Hide('role_dropdown6'),Function(changeRole,6,0)]
    text "{color=#000000}DBA{/color}" xalign 0.81 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.839 yalign 0.99 action [Hide('role_dropdown6'),Function(changeRole,6,1)]
    text "{color=#000000}Developer{/color}" xalign 0.833 yalign 0.98  size 13

screen role_dropdown7():
    imagebutton idle im.Scale("transparent_00.png", 1300, 800) xalign 0.78 yalign 0.005 action [Hide('TransparentScreen_00'), Hide('role_dropdown7')]
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.97 yalign 0.95 action [Hide('role_dropdown7'),Function(changeRole,7,0)]
    text "{color=#000000}DBA{/color}" xalign 0.928 yalign 0.945  size 13
    imagebutton idle im.Scale("panel_lightgreen.png", 100, 30) xalign 0.97 yalign 0.99 action [Hide('role_dropdown7'),Function(changeRole,7,1)]
    text "{color=#000000}Developer{/color}" xalign 0.957 yalign 0.98  size 13
